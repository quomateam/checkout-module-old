<?php

namespace quoma\checkout\components;


/**
 * Description of PlatformFactory
 *
 * @author mmoyano
 */
class PlatformFactory extends \yii\base\Component {

    private static $instance;

    public static function getInstance() {

        if (empty(self::$instance))
            self::$instance = new PlatformFactory;

        return self::$instance;
    }

    /**
     * Devuelve la clase correspondiente a la plataforma de pagos a utilizar
     * @return \class
     * @throws CHttpException
     */
    public function getDefaultPlatform() {

        $class = \Yii::$app>params['default_checkout_platform'];

        return $this->getPlatform($class);
    }

    /**
     * Devuelve la clase correspondiente a la plataforma de pagos a utilizar
     * @return \class
     * @throws CHttpException
     */
    public function getPlatform($name) {

        $platform = \quoma\checkout\models\Platform::find()->where('name="' . $name . '"')->one();
        
        if (empty($platform)) {
            throw new \yii\web\HttpException(500, \Yii::t('app', 'The request platform register does not exist.'));
        }
        
        if (!class_exists($platform->main_class)) {
            throw new \yii\web\HttpException(500, \Yii::t('app', 'The request platform class does not exist.'));
        }

        if (!is_subclass_of($platform->main_class, '\quoma\checkout\components\Platform')) {
            throw new \yii\web\HttpException(500, \Yii::t('app', 'The request platform class is not subclass of components\Platform.'));
        }

        return \Yii::createObject($platform->main_class);
    }

    /**
     * Devuelve una lista con los nombres de las plataformas de pago disponibles.
     * @returns array()
     */
    public function getAllPlatforms() {

        $result = array();

        //Armamos un array con todas las plataformas
        $platforms = \quoma\checkout\models\Platform::find()->where('status="enabled"')->all();

        //verifico si existe el directorio
        foreach ($platforms as $platform) {

            is_readable(\Yii::getAlias('@app') . "/modules/CheckoutModule/platforms/$platform->name/$platform->name.php");

            if (@class_exists($platform->main_class) && is_subclass_of($platform->main_class, '\quoma\checkout\components\Platform'))
                $result[] = new $platform->main_class;
        }

        return $result;
    }

    /**
     * Devuelve una lista con los nombres de las plataformas de pago disponibles.
     * @returns array()
     */
    public function getPlatforms($site) {

        $result = array();

        //Armamos un array con todas las plataformas
        $platforms = $site->platforms;

        //verifico si existe el directorio
        foreach ($platforms as $platform) {

            require_once( \Yii::getAlias('@app') . "/modules/CheckoutModule/platforms/$platform->name/$platform->name.php");

            if (@class_exists($platform->main_class) && is_subclass_of($platform->main_class, '\quoma\checkout\components\Platform'))
                $result[] = new $platform->main_class;
        }

        return $result;
    }

}

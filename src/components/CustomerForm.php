<?php

namespace quoma\checkout\components;


class CustomerForm extends \yii\base\Model {

    public $email; //Utilizado como id único
    public $name;
    public $lastname;
    public $document_type; //Se debe recibir un id de tipo de documento de arya
    public $document_number;
    public $tax_condition; //Se debe recibir un id de condición frente a IVA de arya

    public function rules() {

        return [
            [['name', 'lastname', 'document_number'], 'string'],
            [['tax_condition', 'document_type'], 'number'],
            [['email'], 'email'],
        ];
    }

//    return_url: string,
//	amount: double,
//	datetime: datetime ISO,
//	operation: unique id,
//	details: //opcional, si requiere facturar
//[
//		{concept: string, qty: double, unit_net_price: double, unit_final_price: double},
//		{concept: string, qty: double, unit_net_price: double, unit_final_price: double},
//		...
//],
//invoice: boolean //Se debe facturar?,
//bill_type: integer //Tipos de factura,
//customer: {
//	email: email, //Utilizado como id único
//	name: string,
//	lastname: string,
//	document_type: int, //Se debe recibir un id de tipo de documento de arya
//	document_number: string,
//	tax_condition: int, //Se debe recibir un id de condición frente a IVA de arya
//}
}

<?php

namespace quoma\checkout\components;

use Yii;


/**
 * Maneja los eventos en los pagos 
 */
class EventHandler extends \yii\base\Component {

    /**
     * Evento para mandar a facturar si asi lo desea,
     * @param CEvent $event
     */
    public function onPaid($event) {
         
        //busco el detalle de la request que envio el sistema
//        $data = \quoma\checkout\models\Serialize::getDataByPayment($event->sender->web_payment_id);
//        
//        //Url de retorno
//        $url = $data['return_url'];
//
//        $type = $data['bill_type'];
////        $data['invoice'] = true;
////        $type = 1;
//
//        //si debo facturar
//        if ($data['invoice'] && !empty($type)) {
//
//            $site = \quoma\checkout\models\Site::findOne($data['site_id']);
//            
//            //si todo sale bien
//            if ($bill->close()) {
//
//                if (!empty($site->getCompany()->email)) {
//                    //TODO : Enviar factura al email de la company
//                }
//
//                return false;
//            }
//        }
//        return false;
    }

    public function onError($event) {
        //Flash msg:
        Yii::$app->getSession()->setFlash('msg-pay', 'Ocurrió algún error con el pago.');
    }

    public function onCanceled($event) {
    }

    public function onRejected($event) {
        
    }
     public function onRefunded($event) {
        
    }
     public function onInMediation($event) {
        
    }
     public function onChargedBack($event) {
        
    }
    
    
    public function onPending($event) {
        
    }
}

?>

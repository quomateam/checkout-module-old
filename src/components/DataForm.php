<?php

namespace quoma\checkout\components;

use app\modules\sale\models\BillType;

class DataForm extends \yii\base\Model {
//TODO: recibir un listado de items para armar el detalle de un pago
    public $return_url;
    public $result_url;
    public $amount;
    public $datetime;
    public $operation;
    public $details;
    public $concept;
    public $qty;
    public $unit_net_price;
    public $unit_final_price;
    public $invoice; //Se debe facturar?,
    public $bill_type; //Tipos de factura,
    public $customer;
    public $email; //Utilizado como id único
    public $name;
    public $lastname;
    public $document_type; //Se debe recibir un id de tipo de documento de arya
    public $document_number;
    public $tax_condition; //Se debe recibir un id de condición frente a IVA de arya
    public $site_id;
    public $commerce_number;
    public $expires;
    public $expiration_date_from;
    public $expiration_date_to;
    /* Datos de acceso para Mercado Pago */
    public $client_id;
    public $secret;

    public $installments;
    
    /*Datos para pago con débito*/
    public $payer_email;
    public $reason;
    public $frequency;
    public $frequency_type;
    public $start_date;
    public $end_date;
    public $currency_id;
    public $preapproval_payment;
    /*FIN: Datos para pago con débito*/
             

    /* FIN: Datos de acceso para Mercado Pago */

    public function rules() {

        return [
            [['client_id','installments','preapproval_payment'], 'integer'],
            [['secret','currency_id','start_date'], 'string'],
            [['return_url', 'result_url', 'amount'], 'required'],
            [['bill_type', 'return_url', 'result_url', 'amount'], 'required', 'on' => 'invoice'],
            [['return_url'], 'string', 'max' => 100],
            [['qty', 'site_id', 'invoice', 'commerce_number'], 'number'],
            [['bill_type', 'site_id', 'invoice'], 'integer', 'on' => 'invoice'],
            [['customer', 'datetime', 'operation', 'concept','end_date'], 'safe'],
            [['payer_email','reason','frequency','frequency_type'], 'required', 'on' => 'debit']
        ];
    } 

    public function scenarios() {
        return [
            'default' => ['return_url', 'result_url', 'commerce_number', 'amount', 'qty', 'site_id', 'customer', 'datetime', 'operation', 'concept','secret','client_id','installments','expiration_date_from','expiration_date_to','expires'],
            'invoice' => ['bill_type', 'result_url', 'commerce_number', 'return_url', 'amount', 'site_id', 'invoice','secret','client_id','installments'],
            'debit' => ['return_url', 'result_url', 'commerce_number', 'amount', 'qty', 'site_id', 'customer', 'datetime', 'operation', 'concept','secret','client_id','installments','payer_email', 'reason','frequency','frequency_type', 'start_date', 'end_date','currency_id','preapproval_payment']
        ];
    }
    
    public static function getScenarioName($data) {
      if($data['invoice'] == true){
          return 'invoice';
      }
      
      if(isset($data['preapproval_payment']) && $data['preapproval_payment'] == 1){
          return 'debit';
      }
      
      return 'default';
      
    }

//    return_url: string,
//	amount: double,
//	datetime: datetime ISO,
//	operation: unique id,
//	details: //opcional, si requiere facturar
//      [
//		{concept: string, qty: double, unit_net_price: double, unit_final_price: double},
//		{concept: string, qty: double, unit_net_price: double, unit_final_price: double},
//		...
//      ],
//invoice: boolean //Se debe facturar?,
//bill_type: integer //Tipos de factura,
//customer: {
//	email: email, //Utilizado como id único
//	name: string,
//	lastname: string,
//	document_type: int, //Se debe recibir un id de tipo de documento de arya
//	document_number: string,
//	tax_condition: int, //Se debe recibir un id de condición frente a IVA de arya
//}
}

<?php

namespace quoma\checkout\controllers;

use Yii;
use yii\web\Controller;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\Site;
use quoma\checkout\models\Serialize;
use quoma\checkout\components\PlatformFactory;
use quoma\checkout\models\SiteData;
use quoma\checkout\components\DataForm;
use quoma\checkout\platforms\MercadoPago\MercadoPago;


class DefaultController extends Controller {

    public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }

    public function accessRules() {
        return [
//            ['allow',
//                'actions' => ['beginPayment', 'pay'],
//                'roles' => ['*'],
//            ],
            [
                'allow' => true,
                'actions' => ['beginPayment', 'pay', 'selectPlatform'],
                'roles' => ['?'],
            ],
        ];
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionPaymentsMethods() {

        //request
        $request = \Yii::$app->request;

        if (Yii::$app->params['only_secure_connection'] && $request->isSecureConnection) {
            //url que hace la petición
            $server_name = $request->serverName;
            $methods = array();

            $site = Site::find()->where(['server_name' => $server_name])->one();

            //si el sitio esta registrado
            if ($site !== null) {
                if (empty($site->paymentMethods)) {
                    throw new \yii\web\HttpException(500, 'The site does not have an assigned payment methods.');
                }

                if (!empty($site->paymentMethods)) {
                    foreach ($site->paymentMethods as $method) {
                        $methods[] = $method->name;
                    }
                    return $methods;
                }
            }
        }
    }
    
    /**
     * Inicia el proceso de pago de una factura.
     * En primer lugar genera un objeto WebPayment, que se utiliza para llevar
     * adelante el proceso de pago.
     * Si la factura ya posee un pago asociado, validamos que el estado sea
     * distinto a "payed". Si el estado es "payed" significa que la factura
     * ya fue pagada.
     * @param json $data request
     * 
     */
    public function actionBeginPayment($uuid) {
        
        //request
        $request = \Yii::$app->request;

        if (!Yii::$app->params['only_secure_connection'] || (Yii::$app->params['only_secure_connection'] && $request->isSecureConnection)) {

            $payment = WebPayment::find()->where(['uuid' => $uuid])->one();
           
            //si el sitio esta registrado
            if ($payment !== null) {

                $data = \Yii::$app->request->post();
                
                if (!isset($data['invoice'])){
                    $data['invoice'] = false;
                }
             
                $dataForm = new DataForm(['scenario' => DataForm::getScenarioName($data)]);
                $dataForm->setAttributes($data);
               
                if (!$dataForm->validate()) {
                    $errors = $dataForm->getErrors();
                    $message = '';
                    foreach ($errors as $error)
                        foreach ($error as $error_message)
                            $message .= ' ' . $error_message;

                    throw new \yii\web\HttpException(400, \Yii::t('app', 'Incorrect Parameters.') . $message);
                }
               
                $preapproval_payment = isset($data['preapproval_payment']) ? $data['preapproval_payment'] : '';
                $payment->begin($dataForm, $data['customer'], $data['details'], $data['return_url'], $data['result_url'], $data['operation'],$preapproval_payment);
//                $payment->begin($dataForm->invoice, $dataForm->amount, $dataForm->bill_type, $data['customer'], $data['details'], $data['return_url'], $data['result_url'], $data['operation'], $dataForm->installments);

                if ($payment->save()) {
                    
                    $site = $payment->site;
                    
                    $dataForm->site_id = $site->site_id;
                    
                    //LOG: serializo los datos que viene por post
                    Serialize::saveData($dataForm, $payment->web_payment_id);

                    if (empty($site->platforms)) {
                        throw new \yii\web\HttpException(500, 'The site does not an assigned platform.');
                    }
                    /*
                     * Guardo las credenciales en el sitio del diario los andes para utilizar un único ID de sitio 
                     * en la url de notificaciones de Mercado Pago
                     */
                    $site_losandes = Site::find()->where(['server_name' => Yii::$app->params['cobros_losandes_server_name']])->one();

                    //selección de plataforma de pago
                    if (count($site->platforms) > 1) {
                        SiteData::saveSiteData($dataForm, $site->site_id);
                        SiteData::saveSiteData($dataForm, $site_losandes->site_id);
                        return $this->redirect(['select-platform', 'web_payment_id' => $payment->web_payment_id]);
                    }
                    
                    $platforms = $site->platforms;
                    $platform_id = end($platforms)->platform_id;
                    
                    SiteData::saveSiteData($dataForm, $site->site_id, $platform_id);
                    SiteData::saveSiteData($dataForm, $site_losandes->site_id, $platform_id);
                    
                    return $this->redirect(['pay', 'web_payment_id' => $payment->web_payment_id, 'platform' => $platform_id]);
                    
                }else{
                    throw new \yii\web\HttpException(401, \Yii::t('app', 'Ocurrió un error al crear el pago.'));
                }
            } else {
                throw new \yii\web\HttpException(401, \Yii::t('app', 'The payment does not exist.'));
            }
        } else {
            throw new \yii\web\HttpException(403, \Yii::t('app', 'The request is not sent via secure channel.'));
        }
    }

  
    /**
     * Lleva al usuario a seleccionar la plataforma de pago
     * luego le da el control a la plataforma
     * @param type $web_payment_id
     * @param type $site_id
     * @param type $data
     * @return type
     * TODO: ESTO NO DEBERIA TENER SITE_ID NI DATA
     */
    public function actionSelectPlatform($web_payment_id) {

        $payment = WebPayment::findOne($web_payment_id);
        return $this->render('select_platform', ['platforms' => $payment->site->platforms, 'web_payment_id' => $payment->web_payment_id]);
    }

    public function actionPay($web_payment_id, $platform) {

        $platform_model = \quoma\checkout\models\Platform::findOne($platform);

        $payment = WebPayment::findOne($web_payment_id);
        $payment->platform_id = $platform_model->platform_id;
        $payment->save();
        
        $factory = new PlatformFactory();

        //busco plataformas que usa
        $platform = $factory->getPlatform($platform_model->name);
        
        $platform->pay($payment);
    }

    /**
     * Chequea el estado del pago de un web_payment
     * @return type
     */
    public function actionIsPaid() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                 
        if(!Yii::$app->request->isPost || !isset($_POST['data'])){
            return [
                'status' => 'error',
                'paid' => NULL,
                'message' => 'Error en los datos enviados'
            ];
        }
        

        $data = $_POST['data'];

        $web_payment = WebPayment::find()->where(['request_uuid' => $data])->one();

        if (!$web_payment) {
            return [
                'status' => 'error',
                'paid' => NULL,
            ];
        }
        
        if($web_payment->status == 'paid'){
            return [
                'status' => 'success',
                'paid' => 1,
            ];
        }
        
        
        $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($web_payment->site_id);

        if (!$mp) {
            return [
                'status' => 'error',
                'paid' => NULL,
                'message' => 'No existe la plataforma con nombre "Mercado Pago" para el sitio ' . $web_payment->site->server_name
            ];
        }

        try {
              //Buscamos el pago en Mercado Pago
            $mp_payment = MercadoPago::searchPayments($mp, ["external_reference" => $web_payment->uuid], 0, 10);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }
        

        if (isset($mp_payment['status'])) {
            if ($mp_payment['status'] == 'error') {
                return [
                    'status' => 'error',
                    'message' => $mp_payment['message'],
                ];
            }

            if ($mp_payment['status'] == 'warning') {
                return [
                    'status' => 'success',
                    'paid' => 0,
                ];
            }
        }
        
        
         
        //si hay mas de un pago asociado al reference
        if (count($mp_payment) > 1) {
            $paid = 0;
            foreach ($mp_payment as $key => $payment) {
              
                if ($payment['status'] == 'approved') {
                   $paid = 1;
                }
            }
        } 
       
        if ($mp_payment[0]['collection']['status'] != 'approved' || (isset($paid) && !$paid)) {
            return [
                'status' => 'success',
                'paid' => 0,
            ];
        }
        
        $web_payment->status = 'paid';
        if(!$web_payment->save(['status'])){
            $web_payment->notifyAdmin('Falló salvado de pago en Checkout',"El pago $web_payment->uuid está pagado en ".$web_payment->platform->name." pero no se pudo actualizar el estado en checkout. Que lo disfrutes!");
        }
        
        return [
                'status' => 'success',
                'paid' => 1,
        ];
        
    }

}

<?php

namespace quoma\checkout\controllers;

use Yii;
use quoma\checkout\models\Site;
use quoma\checkout\models\search\SiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteController implements the CRUD actions for Site model.
 */
class SiteController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Site models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Site model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Site model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Site();

        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post()['Site'];

            $rnd = uniqid();
            $uploadedFile = \yii\web\UploadedFile::getInstance($model, 'image');
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name

            if (!empty($uploadedFile)) //si se ha cargado una imagen
                $model->image = $fileName; // le asignoa logo el nombre del archivo generado

            if ($model->validateRelations($post) && $model->save()) {

                if (!empty($uploadedFile)) //si se ha cargado una imagen
                    $uploadedFile->saveAs(Yii::$app->basePath . '/web/images/site/' . $fileName);  // image will uplode to rootDirectory/banner/


                if ($post['platforms_config']) {
                    foreach ($post['platforms_config'] as $platform_id => $data) {
                        $model->setInstallments($data['installments'], $platform_id);
                        $model->setPaymentMethods($data['payment_methods'], $platform_id);
                        $model->setTimeOut($platform_id, $data['time_out']);
                        $model->setTimeOutByPaymentMethodsTypes($data['time_outs_methods'], $platform_id);
                    }
                }

                return $this->redirect(['view', 'id' => $model->site_id]);
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /*
     * 
     */

    public function actionGetConfig() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];

        if ($post = \Yii::$app->request->post()) {

            $installments = \quoma\checkout\models\Installment::find()->all();

            $payment_methods = \quoma\checkout\models\PaymentMethod::find()->where(['platform_id' => $post['select']])->all();

            $payment_method_types = \quoma\checkout\models\PaymentMethodType::find()->all();

            if (isset($post['site_id']))
                $site_has_platform = \quoma\checkout\models\SiteHasPlatform::findOne(['platform_id' => $post['select'], 'site_id' => $post['site_id']]);
            else
                $site_has_platform = NULL;

            $json['status'] = 'success';
            $json['platform_id'] = $post['select'];
            $json['html'] = $this->renderPartial('partials/_view', [
                'installments' => $installments,
                'payment_methods' => $payment_methods,
                'payment_method_types' => $payment_method_types,
                'platform_id' => $post['select'],
                'site_has_platform' => $site_has_platform,
                'site_id' => (isset($post['site_id'])) ? $post['site_id'] : NULL,
            ]);

            return $json;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing Site model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post()['Site'];

            $rnd = uniqid();
            $uploadedFile = \yii\web\UploadedFile::getInstance($model, 'image');
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name

            if (!empty($uploadedFile)) //si se ha cargado una imagen
                $model->image = $fileName; // le asignoa logo el nombre del archivo generado

            if ($model->validateRelations($post) && $model->save()) {

                if (!empty($uploadedFile)) //si se ha cargado una imagen
                    $uploadedFile->saveAs(Yii::$app->basePath . '/web/images/site/' . $fileName);  // image will uplode to rootDirectory/banner/


                    
//                $model->deletePlatforms();
                $model->deleteInstallments();
                $model->deletePaymentMethods();
                $model->deletePaymentMethodTypes();

                if ($post['platforms_config']) {
                    foreach ($post['platforms_config'] as $platform_id => $data) {
                        $model->setInstallments($data['installments'], $platform_id);
                        $model->setPaymentMethods($data['payment_methods'], $platform_id);

                        $model->setTimeOut($platform_id, $data['time_out']);
                        $model->setTimeOutByPaymentMethodsTypes($data['time_outs_methods'], $platform_id);
                    }
                }

                return $this->redirect(['view', 'id' => $model->site_id]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Site model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        $model = $this->findModel($id);

        $model->deletePaymentMethods();
        $model->deletePaymentMethodTypes();
        $model->deleteInstallments();
        $model->deletePlatforms();

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Asignar una empresa de Arya a un sitio
     * @param integer $id Site_id
     * @return mixed
     */
    public function actionAssignCompany($id) {
        return $this->render('assign_company', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Site::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

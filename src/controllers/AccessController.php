<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace quoma\checkout\controllers;

use yii\web\Controller;
use Yii;
use yii\authclient\OAuth2;
use yii\filters\auth\HttpBearerAuth;
use quoma\checkout\models\Site;

/**
 * Description of AccessController
 *
 * @author Gabriela
 */
class AccessController extends Controller {

    private $_site;

    public function init() {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
//            'auth' => function ($server_name) {
//
//                return Site::find()->where(['server_name' => $server_name]);
//            }
        ];
        return $behaviors;
    }

    public function beforeAction($action) {

        // $headers is an object of yii\web\HeaderCollection 
        $headers = Yii::$app->request->headers;
        
        //Valido el token enviado en el header
        if ($headers->has('token')) {
            $token = $headers->get('token');

            $site = Site::find()->where(
                            "server_name='$token'"
                    )->one();

            if (!empty($site)) {
                $this->_site = $site;

                return true;
            }
        }

        parent::beforeAction($action);

        throw new \yii\web\HttpException(401, 'Permiso denegado.');
    }

    public function actionInitPayment() {
        \Yii::$app->response->format = 'json';
        //request
        $request = \Yii::$app->request;
        
        if (!Yii::$app->params['only_secure_connection'] || (Yii::$app->params['only_secure_connection'] && $request->isSecureConnection)) {

            $site = $this->_site;

            //si el sitio esta registrado
            if ($site !== null) {

                $payment = new \quoma\checkout\models\WebPayment();
                $payment->site_id = $site->site_id;
                if(isset(Yii::$app->request->post()['data'])){
                    $payment->request_uuid = Yii::$app->request->post()['data'];
                }

                if ($payment->save()) {

                    return [
                        'status' => 'ok',
                        'uuid' => $payment->uuid,
                        'server_name' => $site->server_name
                    ];
                } else {
                    return [
                        'status' => 'error',
                        'errors' => implode(',', $payment->getErrors())
                    ];
                }
            }
        }
    }

}

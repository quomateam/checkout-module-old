<?php

namespace quoma\checkout\controllers;

use Yii;
use yii\web\Controller;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\Site;
use quoma\checkout\models\Serialize;
use quoma\checkout\components\PlatformFactory;

class CheckoutController extends Controller {

    public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }

    public function actionIndex() {

        $this->render('index');
    }

    /**
     * Inicia el proceso de pago de una factura.
     * En primer lugar genera un objeto WebPayment, que se utiliza para llevar
     * adelante el proceso de pago.
     * Si la factura ya posee un pago asociado, validamos que el estado sea
     * distinto a "payed". Si el estado es "payed" significa que la factura
     * ya fue pagada.
     * @param json $data request
     * 
     */
    public function actionBeginPayment($bill_id) {
        
        $bill = \app\modules\sale\models\Bill::findOne($bill_id);

        //
        if ($bill !== null
                && $bill->status == 'closed'
        ) {

            $payment = new WebPayment();
            $payment->amount = $bill->amount;
            $payment->concept = "";
            //le asigno el id del cliente porq site_id no puede ser nulo
            $payment->site_id = $bill->customer->customer_id;
            $payment->bill_id = $bill->bill_id;

            if ($payment->save()) {
                return $this->redirect(['select-platform', 'web_payment_id' => $payment->web_payment_id]);
            }
        } else {
            throw new \yii\web\HttpException(401, \Yii::t('app', 'The bill does not exist or It was not closed.'));
        }
    }

    /**
     * Lleva al usuario a seleccionar la plataforma de pago
     * luego le da el control a la plataforma
     * @param type $web_payment_id
     * @param type $site_id
     * @param type $data
     * @return type
     * TODO: ESTO NO DEBERIA TENER SITE_ID NI DATA
     */
    public function actionSelectPlatform($web_payment_id) {

        $payment = WebPayment::findOne($web_payment_id);
        
        //Armamos un array con todas las plataformas
        $platforms = \quoma\checkout\models\Platform::find()->where('status="enabled"')->all();
        
        return $this->render('select_platform', ['platforms' => $platforms, 'web_payment_id' => $payment->web_payment_id]);
    }

    public function actionPay($web_payment_id, $platform) {

        $platform_model = \quoma\checkout\models\Platform::findOne($platform);

        $payment = WebPayment::findOne($web_payment_id);
        $payment->platform_id = $platform_model->platform_id;
        $payment->save();

        $factory = new PlatformFactory();

        //busco plataformas que usa
        $platform = $factory->getPlatform($platform_model->name);

        $platform->pay($payment);
    }
    
    
}

<?php

namespace quoma\checkout\commands;

use yii\console\Controller;
use quoma\checkout\models\PaymentMethod;
use quoma\checkout\models\Platform;
use quoma\checkout\platforms\MercadoPago\MercadoPago;
use quoma\checkout\models\Site;

/**
 * Description of MercadoPagoController
 *
 * @author Gabriela
 */
class MercadoPagoController extends Controller {

    public function actionImportPaymentMethods() {

        $platform = Platform::find()->where(['name' => 'Mercado Pago'])->one();

        if (!$platform) {
            echo 'No existe la plataforma con nombre "Mercado Pago"';
            return false;
        }

        $mp = new \MP("TEST-3105433861271210-091909-46aa12b4e74b556622dd8a1cac04463c__LA_LD__-187418877");
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $payment_methods = $mp->get("/v1/payment_methods");

        if ($payment_methods['status'] == '200') {
            foreach ($payment_methods['response'] as $key => $method) {

                $m = PaymentMethod::findOne(['code' => $method['id'], 'platform_id' => $platform->platform_id]);
                
                $payment_method_type = \quoma\checkout\models\PaymentMethodType::findOne(['key' => $method['payment_type_id']]);
               
                if(!$payment_method_type){
                    \Yii::$app->db->createCommand()->insert("$db.payment_method_type", [
                        'name' => $method['payment_type_id'],
                        'key' => $method['payment_type_id'],
                    ])
                    ->execute();
                }
                
                if ($m) {
                    \Yii::$app->db->createCommand()->update("$db.payment_method", [
                                'name' => $method['name'],
                                'code' => $method['id'],
                                'payment_method_type_id' => $payment_method_type->payment_method_type_id,
                                'accreditation_time' => $method['accreditation_time']
                                    ], "payment_method_id=$m->payment_method_id")
                            ->execute();
                } else {
                    \Yii::$app->db->createCommand()->insert("$db.payment_method", [
                                'name' => $method['name'],
                                'code' => $method['id'],
                                'payment_method_type_id' => $payment_method_type->payment_method_type_id,
                                'platform_id' => $platform->platform_id,
                                'accreditation_time' => $method['accreditation_time']
                            ])
                            ->execute();
                }
            }
        }
    }

    public function actionCancelPaymentsByTimeOut() {

        $platform = \quoma\checkout\models\Platform::find()->where(['name' => 'Mercado Pago'])->one();

        if (!$platform) {
            echo 'No existe la plataforma con nombre "Mercado Pago"';
            return false;
        }

        $sites = Site::find()->with('siteHasPlatforms')->all();

        $timeout = '';

        if ($sites) {
            foreach ($sites as $site) {

                $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($site->site_id);

                if (!$mp) {
                    echo 'No existe la plataforma con nombre "Mercado Pago" para el sitio ' . $site->server_name;
                    echo '<br>';
                    continue;
                }
                $sitePlatform = $site->getSiteHasPlatforms()->where(['platform_id' => $platform->platform_id])->one();
                if (!$sitePlatform) {
                    echo 'No existe la plataforma con nombre "Mercado Pago" para el sitio ' . $site->server_name;
                    echo '<br>';
                    continue;
                }

                if ($sitePlatform->time_out == '') {
                    echo 'No se ha configurado un tiempo máximo de vida para las operaciones del sitio ' . $site->server_name;
                    echo '<br>';
                    continue;
                }

                //tiempo máx de vida de una transacción en minutos
                $timeout = $sitePlatform->time_out;

                $currentDate = time();

                //Reservas con mas de x minutos y sin pagar, deben cancelarse
                $firstTime = $currentDate - (60 * $timeout);
                //date_ISO_8601
                $firstTime = date('Y-m-d\TH:i:s', $firstTime) . substr(microtime(), 1, 4) . date('P');

                //Solo revisamos reservas de las ultimas 48hs:
                $fromTime = $currentDate - (48 * 60 * 60);
                //date_ISO_8601
                $fromTime = date('Y-m-d\TH:i:s', $fromTime) . substr(microtime(), 1, 4) . date('P');

                $filters = [
                    "status" => "in_process",
//                    'sort' => 'id',
//                    "range" => "date_created",
//                    "begin_date" => $fromTime,
//                    "end_date" => 'NOW',
                ];

                //Traemos todos los datos juntos porque es muy inestable la función search_payment, devuelve diferentes pagos en cada consulta
                $search_result = $mp->search_payment($filters, 0, 500);
               
                //Contamos la cantidad total[
                $totalCount = $search_result['response']['paging']['total'];
                if ($search_result['status'] == 200 && count($search_result['response']['results']) > 1) {
                    echo 'Total a procesar: ' . $totalCount . "<br>";
                    $result = $search_result['response']['results'];
                    $count = 0;
                    $pageSize = 10;
                    $page = 0;
                    do {
                        //Buscamos mediante IN con un tamanio de pagina igual a $pageSize
                        $in = array_slice($result, $page * $pageSize, $pageSize);
                        
                        foreach ($in as $data) {
                            $payment_data = $data['collection'];
                            $count ++;
                            echo $count . ":<br>";
                            echo 'id: ' . $payment_data['id'] . "<br>";
                            echo 'status: '.$payment_data['status'];
                            // WebPaymentData::savePaymentData($topic, $payment_id, $payment->web_payment_id);
                            //cancelo el pago
//                            $mp->cancel_payment($payment_data['id']);
                            //                        $payment_data = \quoma\checkout\models\WebPaymentData::find()->where(['payment_id' => $payment->web_payment_id])->all();
                        }

                        $page++;
                    } while ($page <= ($totalCount / $pageSize));
                }
            }
        }
    }

}

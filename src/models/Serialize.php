<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "serialize".
 *
 * @property integer $serialize_id
 * @property integer $payment_id
 * @property string $data
 */
class Serialize extends \quoma\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'serialize';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id'], 'integer'],
            [['data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'serialize_id' => Yii::t('app', 'Serialize ID'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'data' => Yii::t('app', 'Data'),
        ];
    }
    
    public static function saveData($data, $payment_id) {
        $serialize = new \quoma\checkout\models\Serialize();
        $serialize->payment_id = $payment_id;
        $serialize->data = serialize($data);
        if ($serialize->save())
            return $serialize->serialize_id;
    }
    
    public static function getData($id) {
        $data = Serialize::findOne($id);
        return ($data) ? unserialize($data->data) : '';
    }
    
    public static function getDataByPayment($id) {
        return unserialize(Serialize::find()->where('payment_id='.$id)->one()->data);
    }
}

<?php

namespace quoma\checkout\models;
use Yii;
use \quoma\checkout\CheckoutModule;

/**
 * This is the model class for table "payment_method_type".
 *
 * @property integer $payment_method_type_id
 * @property string $name
 * @property string $key
 *
 * @property PaymentMethod[] $paymentMethods
 * @property PaymentMethodTypeHasSite[] $paymentMethodTypeHasSites
 */
class PaymentMethodType extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method_type';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'key'], 'string', 'max' => 45],
            [['name', 'key'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_method_type_id' => CheckoutModule::t('Payment Method Type ID'),
            'name' => CheckoutModule::t('Name'),
            'key' => CheckoutModule::t('Key'),
            'paymentMethods' => CheckoutModule::t('Payment Methods'),
            'paymentMethodTypeHasSites' => CheckoutModule::t('Payment Method Type Has Sites'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethods()
    {
        return $this->hasMany(PaymentMethod::className(), ['payment_method_type_id' => 'payment_method_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodTypeHasSites()
    {
        return $this->hasMany(PaymentMethodTypeHasSite::className(), ['payment_method_type_id' => 'payment_method_type_id']);
    }
    
        
        
        
                 
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: PaymentMethods, PaymentMethodTypeHasSites.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}

<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "site_has_platform".
 *
 * @property integer $site_id
 * @property integer $platform_id
 * @property integer $time_out
 *
 * @property Platform $platform
 * @property Site $site
 */
class SiteHasPlatform extends \quoma\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_has_platform';
    }
    
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'platform_id'], 'required'],
            [['site_id', 'platform_id', 'time_out'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => Yii::t('app', 'Site ID'),
            'platform_id' => Yii::t('app', 'Platform ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }
}

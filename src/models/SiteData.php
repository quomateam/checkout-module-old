<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "site_data".
 *
 * @property integer $site_data_id
 * @property integer $site_id
 * @property string $attribute
 * @property string $value
 * @property string $type
 * @property integer $platform_id
 */
class SiteData extends \quoma\core\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout_site_access');
    }

    /**
     * @inheritdoc
     */
    /*
      public function behaviors()
      {
      return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
      ],
      ],
      'date' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
      ],
      'value' => function(){return date('Y-m-d');},
      ],
      'time' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
      ],
      'value' => function(){return date('h:i');},
      ],
      ];
      }
     */

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['site_id'], 'required'],
            [['site_id', 'platform_id'], 'integer'],
            [['attribute', 'value'], 'string', 'max' => 45],
            [['type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'site_data_id' => 'Site Data ID',
            'site_id' => 'Site ID',
            'attribute' => 'Attribute',
            'value' => 'Value',
            'type' => 'Type',
            'platform_id' => 'Plataforma'
        ];
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable() {
        return true;
    }

    /**
     * Guarda los datos de un pago
     * @param type $data
     * @param type $web_payment_id
     */
    public static function saveSiteData($data, $site_id, $platform_id = NULL) {
        $datas = [];

        if ($data->commerce_number) {
            $datas['decidir']['attribute'] = 'commerce_number';
            $datas['decidir']['value'] = $data->commerce_number;
            $datas['decidir']['type'] = 'string';
        }
        if ($data->secret) {
            $datas['mp_secret']['attribute'] = 'secret';
            $datas['mp_secret']['value'] = $data->secret;
            $datas['mp_secret']['type'] = 'string';
        }
        if ($data->client_id) {
            $datas['mp_client_id']['attribute'] = 'client_id';
            $datas['mp_client_id']['value'] = $data->client_id;
            $datas['mp_client_id']['type'] = 'int';
        }

        if ($datas) {
            foreach ($datas as $key => $d) {

                $sd = new SiteData();

                if ($platform_id) {
                    $sd->platform_id = $platform_id;
                }

                $sd->site_id = $site_id;
                $sd->load($d, '');

                //me fijo si ya no existe
                $siteData = SiteData::find()->where(['site_id' => $sd->site_id, 'platform_id' => $sd->platform_id, 'attribute' => $sd->attribute])->one();

                if($siteData) {
                    
                    $siteData->value = $sd->value;
                    $siteData->save(['value']);
                } else {
                    $sd->save();
                }
            }
        }
    }

    public static function findSiteData($site_id, $platform_id = NULL, $attr = NULL) {

        $sd = SiteData::find();
        $sd->where(['site_id' => $site_id]);

        if ($platform_id) {
            $sd->andWhere(['platform_id' => $platform_id]);
        }
        if ($attr) {
            $sd->andWhere(['attribute' => $attr]);
        }
        return $sd->all();
    }

    public static function getAttrSiteData($site_id, $platform_id = NULL, $attr = NULL) {

        $sd = SiteData::find();
        $sd->select('value');
        $sd->where(['site_id' => $site_id]);

        if ($platform_id) {
            $sd->andWhere(['platform_id' => $platform_id]);
        }
        if ($attr) {
            $sd->andWhere(['attribute' => $attr]);
        }
        return $sd->scalar();
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}

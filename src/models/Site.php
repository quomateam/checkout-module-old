<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "site".
 *
 * @property integer $site_id
 * @property string $name
 * @property string $server_name
 * @property string $status
 * @property integer $company_id
 * @property string $image
 * @property string $admin_email
 *
 * @property SiteHasInstallment[] $siteHasInstallments
 * @property Installment[] $installments
 * @property SiteHasPaymentMethod[] $siteHasPaymentMethods
 * @property PaymentMethod[] $paymentMethods
 * @property SiteHasPlatform[] $siteHasPlatforms
 * @property Platform[] $platforms
 * @property WebPayment[] $webPayments
 */
class Site extends \quoma\core\db\ActiveRecord {

    private $_installments;
    private $_paymentMethods;
    private $_platforms;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    /*
      public function behaviors()
      {
      return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
      ],
      ],
      'date' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
      ],
      'value' => function(){return date('Y-m-d');},
      ],
      'time' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
      ],
      'value' => function(){return date('h:i');},
      ],
      ];
      }
     */

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['status','admin_email'], 'string'],
            [['status', 'company_id', 'name', 'server_name','admin_email'], 'required'],
            [['company_id'], 'integer'],
            [['installments', 'paymentMethods', 'platforms'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['server_name'], 'string', 'max' => 100],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'platforms' => \quoma\checkout\CheckoutModule::t('Platforms'),
            'site_id' => \quoma\checkout\CheckoutModule::t('Site'),
            'name' => \quoma\checkout\CheckoutModule::t('Name'),
            'server_name' => \quoma\checkout\CheckoutModule::t('Server Name'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'company_id' => \quoma\checkout\CheckoutModule::t('Company'),
            'company' => \quoma\checkout\CheckoutModule::t('Company'),
            'siteHasInstallments' => 'SiteHasInstallments',
            'installments' => 'Installments',
            'siteHasPaymentMethods' => 'SiteHasPaymentMethods',
            'paymentMethods' => 'PaymentMethods',
            'siteHasPlatforms' => 'SiteHasPlatforms',
            'platforms' => \quoma\checkout\CheckoutModule::t('Platforms'),
            'webPayments' => 'WebPayments',
            'admin_email' => \quoma\checkout\CheckoutModule::t('Admin Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasInstallments() {
        return $this->hasMany(SiteHasInstallment::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstallments() {
        return $this->hasMany(Installment::className(), ['installment_id' => 'installment_id'])->viaTable('site_has_installment', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasPaymentMethods() {
        return $this->hasMany(SiteHasPaymentMethod::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethods() {
        return $this->hasMany(PaymentMethod::className(), ['payment_method_id' => 'payment_method_id'])->viaTable('site_has_payment_method', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodTypes() {
        return $this->hasMany(PaymentMethodType::className(), ['payment_method_type_id' => 'payment_method_type_id'])->viaTable('payment_method_type_has_site', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodTypeHasSite() {
        return $this->hasMany(PaymentMethodTypeHasSite::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasPlatforms() {
        return $this->hasMany(SiteHasPlatform::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatforms() {
        return $this->hasMany(Platform::className(), ['platform_id' => 'platform_id'])->viaTable('site_has_platform', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayments() {
        return $this->hasMany(WebPayment::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(\app\modules\sale\models\Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * @brief Sets Installments relation on helper variable and handles events insert and update
     */
    public function setInstallments($installments, $platform_id) {

        if (empty($installments)) {
            $installments = [];
        }

        foreach ($installments as $id) {
            $this->link('installments', Installment::findOne($id), ['platform_id' => $platform_id]);
        }
    }

    /**
     * @brief Sets Installments relation on helper variable and handles events insert and update
     */
    public function setTimeOut($platform_id, $time_out) {

        $site_has_platform = SiteHasPlatform::findAll(['platform_id' => $platform_id, 'site_id' => $this->site_id]);

        if ($site_has_platform)
            foreach ($site_has_platform as $site_platform) {
                if ($site_platform->platform_id == $platform_id) {
                    $site_platform->time_out = $time_out;
                    $site_platform->save(['time_out']);
                }
            }
    }

    /**
     * @brief Sets PaymentMethods relation on helper variable and handles events insert and update
     */
    public function setTimeOutByPaymentMethodsTypes($paymentMethodsTypes, $platform_id) {

        if (empty($paymentMethodsTypes)) {
            $paymentMethodsTypes = [];
        }


        foreach ($paymentMethodsTypes as $method_type_id => $time_out) {
            if ($time_out) {
                $this->link('paymentMethodTypes', PaymentMethodType::findOne($method_type_id), ['platform_id' => $platform_id, 'time_out' => $time_out]);
            }
        }
    }

    /**
     * Elimina  las relaciones del sitio con installment
     */
    public function deleteInstallments() {
        $old_installments = Installment::find()->joinWith('siteHasInstallments', true)->where(['site_id' => $this->site_id])->all();

        if ($old_installments) {
            foreach ($old_installments as $model) {
                $this->unlink('installments', $model, true);
            }
        }
    }

    /**
     * @brief Sets PaymentMethods relation on helper variable and handles events insert and update
     */
    public function setPaymentMethods($paymentMethods, $platform_id) {

        if (empty($paymentMethods)) {
            $paymentMethods = [];
        }


        foreach ($paymentMethods as $id) {
            $this->link('paymentMethods', PaymentMethod::findOne($id), ['platform_id' => $platform_id]);
        }
    }

    /**
     * Elimina  las relaciones del sitio con installment
     */
    public function deletePaymentMethods() {
        $old_payment = PaymentMethod::find()->joinWith('siteHasPaymentMethods', true)->where(['site_id' => $this->site_id])->all();

        if ($old_payment) {
            foreach ($old_payment as $model) {
                $this->unlink('paymentMethods', $model, true);
            }
        }
    }

    /**
     * Elimina  las relaciones del sitio con installment
     */
    public function deletePaymentMethodTypes() {
        $old_methods_types = PaymentMethodType::find()->joinWith('paymentMethodTypeHasSites', true)->where(['site_id' => $this->site_id])->all();

        if ($old_methods_types) {
            foreach ($old_methods_types as $model) {
                $this->unlink('paymentMethodTypes', $model, true);
            }
        }
    }

    /**
     * Elimina  las relaciones del sitio con installment
     */
    public function deletePlatforms() {

        if ($this->platforms) {
            $this->unlinkAll('platforms', true);
        }
    }

    /**
     * @brief Sets Platforms relation on helper variable and handles events insert and update
     */
    public function setPlatforms($platforms) {

        if (empty($platforms)) {
            $platforms = [];
        }

        $this->_platforms = $platforms;

        $savePlatforms = function($event) {
            $this->unlinkAll('platforms', true);

            foreach ($this->_platforms as $id) {
                $this->link('platforms', Platform::findOne($id));
            }
        };

        $this->on(self::EVENT_AFTER_INSERT, $savePlatforms);
        $this->on(self::EVENT_AFTER_UPDATE, $savePlatforms);
    }

    /**
     * @inheritdoc
     * Strong relations: SiteHasInstallments, Installments, SiteHasPaymentMethods, PaymentMethods, SiteHasPlatforms, Platforms, WebPayments.
     */
    public function getDeletable() {


//        if ($this->getInstallments()->exists()) {
//            return false;
//        }
//       
//        if ($this->getPaymentMethods()->exists()) {
//            return false;
//        }
//       
//        if ($this->getPlatforms()->exists()) {
//            return false;
//        }
        if ($this->getWebPayments()->exists()) {
            return false;
        }
        return true;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillPaymentMethods($platform_id) {

        $select = [];

        $paymentMethods = $this->getSiteHasPaymentMethods()->andWhere(['platform_id' => $platform_id])->all();

        if ($paymentMethods)
            foreach ($paymentMethods as $model) {
                if (isset($model->payment_method_id))
                    $select[] = $model->payment_method_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillPaymentMethodTypes($platform_id) {

        $select = [];

        $paymentMethodTypes = $this->getPaymentMethodTypeHasSite()->andWhere(['platform_id' => $platform_id])->all();

        if ($paymentMethodTypes)
            foreach ($paymentMethodTypes as $model) {
                if (isset($model->payment_method_type_id))
                    $select[] = $model->payment_method_type_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function findTimeOutPaymentMethodTypes($platform_id, $payment_method_type_id) {

        $paymentMethodType = $this->getPaymentMethodTypeHasSite()->andWhere(['platform_id' => $platform_id, 'payment_method_type_id' => $payment_method_type_id])->one();

        if ($paymentMethodType){
            return $paymentMethodType->time_out;
        }
        
        return NULL;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function paymentMethodIsSelected($selected, $payment_method_id) {

        if ($selected)
            foreach ($selected as $id) {
                if ($id == $payment_method_id) {
                    return true;
                }
            }

        return false;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function getTimeOut($platform_id, $payment_method_id) {

        $select = [];

        $time_out = $this->getSiteHasPaymentMethods()
                ->select('time_out')
                ->andWhere(['platform_id' => $platform_id, 'payment_method_id' => $payment_method_id])
                ->scalar();
        return $time_out;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillInstallments($platform_id) {

        $select = [];

        $installments = $this->getSiteHasInstallments()->andWhere(['platform_id' => $platform_id])->all();

        if ($installments)
            foreach ($installments as $model) {
                if (isset($model->installment_id))
                    $select[] = $model->installment_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillPlataforms() {

        $select = [];

        $platforms = $this->_platforms;
        if (!$platforms)
            $platforms = $this->platforms;

        if ($platforms)
            foreach ($platforms as $model) {
                if (isset($model->platform_id))
                    $select[] = $model->platform_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Valida que el usuario haya seleccionado todas las relaciones necesarias
     */
    public function validateRelations($post) {

        if (empty($post['platforms'])) {
            $this->addError('platforms', 'Plataforma no puede estar vacío.');
            return false;
        }

        if (!isset($post['platforms_config'])) {
            $this->addError('platforms', 'Debe seleccionar los métodos de pago.');
            return false;
        }

        if (count($post['platforms']) != count($post['platforms_config'])) {
            $this->addError('platforms', 'Debe seleccionar los métodos de pago.');
            return false;
        }

        foreach ($post['platforms_config'] as $platform_id => $data) {

            if (!isset($data['payment_methods'])) {
                $this->addError('platforms', 'Debe seleccionar los métodos de pago.');
                return false;
            }
            if (!isset($data['installments'])) {
                $this->addError('platforms', 'Debe seleccionar las cuotas permitidas.');
                return false;
            }
        }

        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        
    }

    public static function getStatus() {
        return [
            'enabled' => \quoma\checkout\CheckoutModule::t('Enabled'),
            'disabled' => \quoma\checkout\CheckoutModule::t('Disabled'),
        ];
    }

    public function getLabelStatus() {
        return $this->getStatus()[$this->status];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @param type $platform_id
     * @return type
     */
    public function getExcludedPaymentMethods($platform_id) {
        $payment_methods = [];

        if ($this->paymentMethods) {
            foreach ($this->paymentMethods as $method) {
                $payment_methods[] = $method->payment_method_id;
            }
        }
        $excluded_code = [];

        $excluded = PaymentMethod::find()
                ->select('payment_method.code')
                ->joinWith('siteHasPaymentMethods')
                ->where(['not in', 'payment_method.payment_method_id', $payment_methods])
                ->andWhere("payment_method.platform_id=$platform_id")
                ->all();

        if ($excluded) {
            foreach ($excluded as $e) {
                $excluded_code[] = ['id' => $e->code];
            }
        }

        return $excluded_code;
    }

    /*
     * 
     */

    public function findInstallment() {

        return $this->getInstallments()->max('qty');
    }

}

<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "platform".
 *
 * @property integer $platform_id
 * @property string $main_class
 * @property string $status
 * @property string $name
 *
 * @property SiteHasPlatform[] $siteHasPlatforms
 * @property Site[] $sites
 */
class Platform extends \quoma\core\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'platform';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['status', 'name', 'main_class'], 'required'],
            [['status', 'name'], 'string'],
            [['main_class'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [

            'platform_id' => \quoma\checkout\CheckoutModule::t('Platform'),
            'main_class' => \quoma\checkout\CheckoutModule::t('Main Class'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'name' => \quoma\checkout\CheckoutModule::t('Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasPlatforms() {
        return $this->hasMany(SiteHasPlatform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites() {
        return $this->hasMany(Site::className(), ['site_id' => 'site_id'])->viaTable('site_has_platform', ['platform_id' => 'platform_id']);
    }

    public static function getStatus() {
        return [
            'enabled' => \quoma\checkout\CheckoutModule::t('Enabled'),
            'disabled' => \quoma\checkout\CheckoutModule::t('Disabled'),
        ];
    }

    public function getLabelStatus() {
        return $this->getStatus()[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayments() {
        return $this->hasMany(WebPayment::className(), ['platform_id' => 'platform_id']);
    }

    public function getDeletable() {
        if ($this->getSiteHasPlatforms()->exists()) {
            return false;
        }

        if ($this->getWebPayments()->exists()) {
            return false;
        }

        return true;
    }

}

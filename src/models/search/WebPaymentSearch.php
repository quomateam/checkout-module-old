<?php

namespace quoma\checkout\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\models\WebPayment;

/**
 * WebPaymentSearch represents the model behind the search form about `quoma\checkout\models\WebPayment`.
 */
class WebPaymentSearch extends WebPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['web_payment_id', 'platform_id', 'invoice', 'site_id', 'bill_id'], 'integer'],
            [['status', 'message', 'uuid', 'concept', 'return_url'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WebPayment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'web_payment_id' => $this->web_payment_id,
            'amount' => $this->amount,
            'platform_id' => $this->platform_id,
            'invoice' => $this->invoice,
            'site_id' => $this->site_id,
            'bill_id' => $this->bill_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'uuid', $this->uuid])
            ->andFilterWhere(['like', 'concept', $this->concept])
            ->andFilterWhere(['like', 'return_url', $this->return_url]);

        return $dataProvider;
    }
}

<?php

namespace quoma\checkout\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\models\Platform;

/**
 * PlatformSearch represents the model behind the search form about `app\modules\checkout\models\Platform`.
 */
class PlatformSearch extends Platform {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['platform_id'], 'integer'],
            [['main_class', 'status', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Platform::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'platform_id' => $this->platform_id,
        ]);

        $query->andFilterWhere(['like', 'main_class', $this->main_class])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

}

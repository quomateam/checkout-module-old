<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "payment_method_type_has_site".
 *
 * @property integer $payment_method_type_id
 * @property integer $site_id
 * @property integer $platform_id
 * @property integer $time_out
 *
 * @property PaymentMethodType $paymentMethodType
 * @property Platform $platform
 * @property Site $site
 */
class PaymentMethodTypeHasSite extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method_type_has_site';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_method_type_id', 'site_id', 'platform_id'], 'required'],
            [['payment_method_type_id', 'site_id', 'platform_id', 'time_out'], 'integer'],
            [['paymentMethodType', 'platform', 'site'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_method_type_id' => 'Payment Method Type ID',
            'site_id' => 'Site ID',
            'platform_id' => 'Platform ID',
            'time_out' => 'Time Out',
            'paymentMethodType' => 'PaymentMethodType',
            'platform' => 'Platform',
            'site' => 'Site',
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodType()
    {
        return $this->hasOne(PaymentMethodType::className(), ['payment_method_type_id' => 'payment_method_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }
    
        
        
        
        
        
                     
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: PaymentMethodType, Platform, Site.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}

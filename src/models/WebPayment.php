<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "web_payment".
 *
 * The followings are the available columns in table 'web_payment':
 * @property string $web_payment_id
 * @property string $status
 * @property string $message
 * @property double $amount
 * @property string $uuid
 * @property string $concept
 * @property integer $platform_id
 * @property string $return_url
 * @property integer $invoice
 * @property integer $site_id
 * @property integer $bill_id
 * @property string $result_url
 * @property string $request_uuid
 * @property integer $installments
 * @property string $payer_email
 * @property integer $frequency
 * @property string $currency_id
 * @property string $payment_start_date
 * @property string $payment_end_date
 * @property string $reason
 * 
 *
 * The followings are the available model relations:
 * @property Bill $bill
 * @property WebReceipt[] $receipts
 */
class WebPayment extends \quoma\core\db\ActiveRecord {

    const TOLERANCE = '0.01';

    /* Dato del comercio para conectarse con Decidir */

    protected $commerce_number;

    /* Datos del comercio para conectarse con Mercado Pago */
    protected $client_id;
    protected $secret;

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return 'web_payment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * Generamos un id unico para luego efectuar un pago
     */
    public function init() {
        $this->on('onPay', ['\quoma\checkout\components\EventHandler', 'onPay']);
        $this->on('onError', ['\quoma\checkout\components\EventHandler', 'onError']);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            [['status', 'currency_id', 'payment_start_date', 'payment_end_date', 'reason', 'external_status'], 'string'],
            [['platform_id', 'site_id', 'bill_id', 'installments', 'frequency'], 'integer'],
            [['status'], 'string', 'max' => 8],
            [['message', 'concept'], 'string', 'max' => 255],
            [['return_url'], 'string', 'max' => 100],
            [['uuid', 'request_uuid','expiration_date_from','expiration_date_to'], 'string', 'max' => 45],
            [['amount'], 'double'],
            [['invoice','expires'], 'boolean'],
            [['payer_email'], 'email'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            [['web_payment_id', 'status', 'message', 'amount', 'uuid', 'concept', 'site_id', 'invoice', 'return_url', 'result_url'], 'safe', 'on' => 'search'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'web_payment_id' => \quoma\checkout\CheckoutModule::t('Web Payment'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'message' => \quoma\checkout\CheckoutModule::t('Message'),
            'amount' => \quoma\checkout\CheckoutModule::t('Amount'),
            'uuid' => \quoma\checkout\CheckoutModule::t('UUID'),
            'concept' => \quoma\checkout\CheckoutModule::t('Concept'),
            'return_url' => \quoma\checkout\CheckoutModule::t('Return Url'),
            'site_id' => \quoma\checkout\CheckoutModule::t('Site'),
            'site' => \quoma\checkout\CheckoutModule::t('Site'),
            'invoice' => \quoma\checkout\CheckoutModule::t('Invoice'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipts() {
        return $this->hasMany(WebReceipt::className(), ['web_payment_id' => 'web_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform() {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill() {
        return $this->hasMany(\app\modules\sale\models\Bill::className(), ['bill_id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedReceipts() {
        return $this->hasMany(WebReceipt::className(), ['web_payment_id' => 'web_payment_id'])->andWhere('status="approved"');
    }

    /**
     * Returns true if the object is being used and can not be deleted. Otherwise returns false.
     * @return boolean
     */
    public function isUsed() {

        if (empty($this->receipts))
            return false;
        else
            return true;
    }

    public function beforeSave($insert) {
        if (empty($this->uuid)) {
            $this->uuid = uniqid('', true);
        }

        if (empty($this->status)) {
            $this->status = 'pending';
        }

        if ($insert) {
            $this->create_date = date('Y-m-d');
            $this->create_time = date('H:i:s');
            $this->create_datetime = time();
            $this->type = 'payment';
        }
//            $this->update_date = date('Y-m-d');
//            $this->update_time = date('H:i:s');
//            $this->update_datetime = time();

        return parent::beforeSave($insert);
    }

    /**
     * No permitimos que el objeto sea eliminado si esta siendo usado.
     * @return boolean
     */
    public function beforeDelete() {

        if ($this->isUsed)
            return false;

        return parent::beforeDelete();
    }

    /**
     * Controla que el importe abonado corresponda al importe del pago, con
     * una tolerancia defeinida por self::TOLERANCE
     * @return boolean
     */
    public function check() {

        $total = 0.0;

        foreach ($this->approvedReceipts as $receipt)
            $total += (float) $receipt->amount;

        $total_tolerated = (1 + self::TOLERANCE) * $total;

        //Validamos que el importe abonado corresponda con el importe de la factura
        if ($this->amount <= $total_tolerated)
            return true;

        return false;
    }

    /**
     * Devuelve el saldo pendiente de pago
     * @return boolean
     */
    public function getPendingAmount() {

        $paid = 0.0;
        foreach ($this->approvedReceipts as $receipt)
            $paid += (float) $receipt->amount;

        return $this->amount - $paid;
    }

    /*
     * Cambia el estado del pago
     */

    public function changeStatus($status) {

        $this->status = $status;

        if ($this->save(true, array('status'))) {

            if ($this->status == 'in_mediation') {
                $this->onInMediation(new \yii\base\Event(['sender' => $this]));
            } elseif ($this->status == 'charged_back') {
                $this->onChargedBack(new \yii\base\Event(['sender' => $this]));
            } else {
                $event_name = 'on' . ucfirst($this->status);
                $this->$event_name(new \yii\base\Event(['sender' => $this]));
            }

            $this->update_date = date('Y-m-d');
            $this->update_time = date('H:i:s');
            $this->update_datetime = time();
            $this->save(['update_date', 'update_time', 'update_datetime']);
        }
    }

    /**
     * Recibo del pago
     * @param array $data
     * @return boolean
     */
    public function receipt($data) {

        //Generamos un recibo
        $receipt = new \quoma\checkout\models\WebReceipt();
        $receipt->web_payment_id = $this->web_payment_id;

        //Cargamos los datos recibidos
        $receipt->status = $data['status'];
        $receipt->amount = $data['amount'];
        $receipt->currency = $data['currency'];
        $receipt->autorization_code = $data['autorization_code'];
        $receipt->installments = $data['installments'];
        $receipt->owner = $data['owner'];
        $receipt->card = $data['card'];
        $receipt->email = $data['email'];
        $receipt->datetime = ($data['datetime'] == '') ?  $data['datetime'] : date('Y-m-d H:i:s');
        $receipt->operation_number = $this->uuid;

        //Al guardar, cambiamos el estado del pago segun si el pago fue completo o no
        if ($receipt->save()) {

            $this->external_status = $receipt->status;
            $this->save(false, ['external_status']);
            
            $status = $receipt->status;
           
            //Estado
            if ($receipt->status == 'in_process' || $receipt->status == 'authorized') {
                $status = 'pending';
            } elseif ($receipt->status == 'cancelled') {
                $status = 'canceled';
            }

            //Pago aprobado
            if ($receipt->status == 'approved') {
                //si el pago ya ha sido cancelado en cobros le notifico al administrador para que verifique si hubo sobreventa
                if (in_array($this->status, ['canceled', 'timeout', 'error'])) {
                    $this->sendMail(Yii::$app->params['adminEmail']);
                } 
                
                if ($this->check()) {

                    $this->changeStatus('paid');
                } else {
                    $this->changeStatus('partial');
                }

                return 'approved';
            } else {
                
                $status = $status;
                if($receipt->status == 'unapproved'){
                    $status = 'error';
                }
                //Pago no aprobado
                $this->changeStatus($status);
                return 'error';
            }
        } else {
            $this->changeStatus('error');

            $message = '';
            foreach ($receipt->getErrors() as $error) {
                $message .= $error[0];
            }

            $this->message = $message;
            $this->save();
            return $message;
        }
    }
    
    public function notifyAdmin($subject, $body) {

        Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo(Yii::$app->params['adminEmail'])
                ->setSubject($subject)
                ->setTextBody($body)
                ->send();
    }

    public function sendMail($to) {

        $body = 'El pago ' . $this->uuid . " fue aprobado por ". $this->platform->name."pero el mismo fue cancelado previamente.\n\n";
        $body .= "Por favor pedimos revisarlo.\n\n";
        $body .= "Datos del Pago:\n";
        $body .= ' Nro de operación en Cobros: ' . $this->uuid . "\n";
        $body .= ' Nro de operación en ' . $this->site->name . ': ' . $this->request_uuid . "\n";
        $body .= ' Estado: ' . $this->status . "\n";

        Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($to)
                ->setSubject('URGENTE! Ocurrió algún error en un pago.')
                ->setTextBody($body)
                ->send();
    }

    /**
     * @brief Obtiene el ultimo datetime de todos los receipts aprobados del pago
     * @return type
     */
    public function getLastReceiptTimeout() {

        $receipts = $this->approvedReceipts;

        $datetime = 0;

        if (!empty($receipts))
            foreach ($receipts as $key => $receipt)
                if ($receipt->datetime > $datetime)
                    $datetime = $receipt->datetime;

        return $datetime;
    }

    /**
     * @brief Chequea si el pago no ha superado el tiempo de vida máximo
     * @return boolean
     */
    public function isOnTimeout() {

        $timeout = $this->create_datetime + (60 * (Yii::$app->params['time_cobros'] - 1)) - 1;
        $now = time();

        if ($now > $timeout)
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Devuelve el tiempo en minutos (timeout) configurado para el sitio
     * @return int
     */
    public function getTimeOut($payment_method_key) {
        $time_out = 0;
        $site = $this->site;

        $site_has_platform = $site->getSiteHasPlatforms()->where(['platform_id' => $this->platform_id])->one();

        if ($site_has_platform && $site_has_platform->time_out != '') {
            $time_out = $site_has_platform->time_out;
        } else {
            $payment_method_type_has_site = $site->getPaymentMethodTypeHasSite()->where(['platform_id' => $this->platform_id, 'payment_method_type_id' => $payment_method_key])->one();
            if ($payment_method_type_has_site && $payment_method_type_has_site->time_out != '') {
                $time_out = $payment_method_type_has_site->time_out;
            }
        }

        return $time_out;
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "paid"
     * @param type $event
     */
    public function onPaid($event) {
        $this->trigger('onPaid');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onError($event) {
        $this->trigger('onError');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onCanceled($event) {
        $this->trigger('onCanceled');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onTimeout($event) {
        $this->trigger('onTimeout');
    }

    public function onRejected($event) {
        $this->trigger('onRejected');
    }

    public function onRefunded($event) {
        $this->trigger('onRefunded');
    }

    public function onInMediation($event) {
        $this->trigger('onInMediation');
    }

    public function onChargedBack($event) {
        $this->trigger('onChargedBack');
    }
    
    public function onPending($event) {
        $this->trigger('onPending');
    }

    /**
     * 
     * @param type $invoice
     * @param type $amount
     * @param type $type
     * @param type $site
     * @param type $customer_data
     * @param type $details
     * @param type $return_url
     * @param type $result_url
     * @param type $operation uuid del payment en el sitio externo
     * @throws \yii\web\HttpException
     */
    public function begin($data, $customer_data = [], $details = [], $return_url, $result_url, $operation_nro, $preapproval_payment) {

        if ($data->invoice) {

            if (!$this->site->getCompany()->exists()) {
                throw new \yii\web\HttpException(401, \Yii::t('app', 'The site does not have an assigned company.'));
            }

            //Creo la factura según el tipo
            $bill = \app\modules\sale\components\BillExpert::createBill($data->bill_type);

            $customerForm = new \quoma\checkout\components\CustomerForm;
            $customerForm->setAttributes($customer_data);

            if (!$customerForm->validate()) {
                throw new \yii\web\HttpException(400, \Yii::t('app', 'Incorrect Parameters.'));
            }

            //Busco si existe el cliente, Email usado como id único
            $customer = \app\modules\sale\models\Customer::find()->where('email="' . $customerForm->email . '"')->one();

            //lo creo si no existe
            if ($customer == NULL) {


                $customer = new \app\modules\sale\models\Customer;
                $customer->link('documentType', \app\modules\sale\models\DocumentType::findOne($customerForm->document_type));
                $customer->link('taxCondition', \app\modules\sale\models\TaxCondition::findOne($customerForm->tax_condition));

                if ($bill->billType && !$customer->checkBillType($bill->billType)) {
                    throw new \yii\web\HttpException(400, \Yii::t('app', 'Current customer requires "{billType}.'));
                    $this->addError('bill_type_id', Yii::t('app', 'Current customer requires "{billType}"'));
                }

                $customer->company_id = $this->site->company_id;
                $customer->setAttributes($customerForm);

                $customer->save();
            }

            //asigno el cliente
            $bill->customer = $customer;

            //agrego el detalle de la factura
            if (!empty($details)) {
                foreach ($details as $detail) {
                    $bill->addDetail($detail);
                }
            }

            $bill->save();
            $this->bill_id = $bill->bill_id;

            if ($bill->calculateAmount() != $data->amount) {
                throw new \yii\web\HttpException(400, \Yii::t('app', 'The amounts do not match.'));
            }
        }

        $this->amount = $data->amount;
        $this->return_url = $return_url;
        $this->result_url = $result_url;
        $this->concept = $data->concept;
        $this->request_uuid = $operation_nro;
        $this->installments = $data->installments;
        $this->payer_email = $data->payer_email;
        $this->frequency = $data->frequency;
        $this->frequency_type = $data->frequency_type;
        $this->currency_id = $data->currency_id;
        $this->payment_start_date = $data->start_date;
        $this->payment_end_date = $data->end_date;
        $this->reason = $data->reason;
        $this->preapproval_payment = $preapproval_payment;
        $this->expires = ($data->expires == true) ? 1 : 0;

        if($this->preapproval_payment){
            $this->type = 'preapproval';
        }
    }

    /*
     * Devuelve el uuid del pago consultado a Mercado Pago con el $preapproval_id
     */

    public static function getPaymentInfo($site_id, $preapproval_id) {

        $platform = \quoma\checkout\platforms\MercadoPago\MercadoPago::getPlatform();
        $site = Site::findOne($site_id);

        $secret = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'secret');
        $client_id = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'client_id');

        $mp = new \MP($client_id, $secret);

        $payment_info = $mp->get_preapproval_payment($preapproval_id);

        if ($payment_info["status"] == 200) {
            return $payment_info;
        }
    }

    /**
     * Crea una subsacripción para pagos con debito automatico
     * @param type $response
     * @return \quoma\checkout\models\Subscription
     */
    public static function getSubscription($response) {
        //Obtengo el uuid del pago desde el external_reference del pago . 
        //TODO: Cambiar esto!!!!En los pagos con debito se debe crear una suscripción y los pagos se deben ir creando a medida que 
        //Mercado pago envia notificaciones
//        $uuid = WebPayment::getSubscriptionUuid($response['external_reference']);
       
        //busco si la suscripción ya existe  sino la creo
        $subscription = Subscription::find()->where(['uuid' => $response['external_reference']])->one();
       
        if (!$subscription) {
            $subscription = new Subscription();
            $subscription->status = 'pending';
        }

        $subscription->load($response, '');
        $subscription->frequency = (string)$response['auto_recurring']['frequency'];
        $subscription->frequency_type = $response['auto_recurring']['frequency_type'];
        $subscription->transaction_amount = $response['auto_recurring']['transaction_amount'];
        $subscription->currency_id = $response['auto_recurring']['currency_id'];
        $subscription->start_date = (isset($response['auto_recurring']['end_date'])) ? $response['auto_recurring']['end_date'] : '';
        $subscription->end_date = (isset($response['auto_recurring']['end_date'])) ? $response['auto_recurring']['end_date'] : NULL;
        if (isset($response['id'])) {
            $subscription->external_id = $response['id'];
        }
        $subscription->uuid = $response['external_reference'];
        
        if(isset($response['status'])){
             $subscription->status = $response['status'];
        }
        
         if(isset($response['payer_id'])){
             $subscription->payer_id = (string)$response['payer_id'];
        }
      
        if ($subscription->save()) {
            return $subscription;
        }else{
           
            \Yii::info($subscription);
            \Yii::info($subscription->getErrors());
        }
        return false;
    }

    public static function getPaymentUuid($external_reference) {
        $position = strpos($external_reference, '-');
        $uuid = substr($external_reference, $position+1);
        return $uuid;
    }
    
     public static function getSubscriptionUuid($external_reference) {
        $position = strpos($external_reference, '-');
        $uuid = substr($external_reference, 0,$position);
        return $uuid;
    }

    /**
     * Notifica a por curl a un sitio los parametros pasados
     * @param type $payment
     * @param type $data
     * @return type
     */
    public static function NotifyToSite($payment, $data = NULL) {
        
        //open connection
        $ch = curl_init();
       
        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        //si tengo que enviar parametros por post
        if ($data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if (Yii::$app->params['testing']) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        //execute post
        $result = curl_exec($ch);

        if ($result === false) {
            $result = curl_error($ch);
        }
        //close connection
        curl_close($ch);
        
        Serialize::saveData($result, $payment->web_payment_id);
       
        return $result;
    }
    
    public function getExpirationDateFrom(){
        
    }
    
    public function getExpirationDateTo() {
        $site_platform = $this->site->getSiteHasPlatforms()->where(['platform_id' => $this->platform_id])->one();
        //tiempo máx de vida de una transacción . Minutos
        $timeout = $site_platform->time_out;
        
        if(!$timeout){
            return NULL;
        }
       
        $expiration_date =  strtotime("+$timeout minutes",$this->create_datetime);
        $date_ISO_8601 = date('Y-m-d\TH:i:s', $expiration_date) . substr(microtime(), 1, 4) . date('P');
        $this->expiration_date_to = $date_ISO_8601;
        $this->save(['expiration_date_to']);
        
        return $date_ISO_8601;
    }

}

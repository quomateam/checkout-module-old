<?php

namespace quoma\checkout\platforms\MercadoPago;

use Yii;
use quoma\checkout\models\SiteData;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MercadoPago
 *
 * @author Gabriela
 */
class MercadoPago extends \quoma\checkout\components\Platform implements \quoma\checkout\components\CheckoutInterface {

    private $mp;
    private $access_token;
    public $config;
    public $client_id;
    public $secret;

    public function renderForm($payment) {
        
    }
    public static function getPlatform(){
        return \quoma\checkout\models\Platform::find()->where(['name' => 'Mercado Pago'])->one();
    }

    /**
     * Retorna una instancia de mercadopago para hacer los request al mismo.
     *
     * @return \MP
     */
    public static function getMercadoPago($site_id) {
        
        //busco los datos que envió el sitio 
        $client_id = SiteData::getAttrSiteData($site_id, self::getPlatform()->platform_id, 'client_id');
        $secret = SiteData::getAttrSiteData($site_id, self::getPlatform()->platform_id, 'secret');;

        $mp = new \MP(\Yii::$app->params['testing'] ? \Yii::$app->params['client_id'] : $client_id, \Yii::$app->params['testing'] ? \Yii::$app->params['secret'] : $secret);
        $mp->sandbox_mode(\Yii::$app->params['mp_sandbox']);
        return $mp;
    }

    public function pay($payment) {
        if($payment->preapproval_payment){
            return Yii::$app->response->redirect(
                                    Yii::$app->urlManager->createUrl(['checkout_online/MercadoPago/default/create-preapproval-payment', 'payment' => $payment->uuid])
                    );
        }
        return Yii::$app->response->redirect(
                        Yii::$app->urlManager->createUrl(['checkout_online/MercadoPago/default/create-preference', 'payment' => $payment->uuid])
        );
    }

    public function getCallbackUrl() {
        if(\Yii::$app->params['only_secure_connection']){
            return \Yii::$app->urlManager->createAbsoluteUrl('','https') . 'checkout_online/MercadoPago/default/back';
        }else{
            return \Yii::$app->urlManager->createAbsoluteUrl('','http') . 'checkout_online/MercadoPago/default/back';
        }
    }
    
    public static function searchPayments($mp, $filters, $offset, $limit) {

        try {
            //Buscamos el pago en Mercado Pago
            $search_result = $mp->search_payment($filters, $offset, $limit);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => $exc->getTraceAsString()
            ];
        }
        
        if ($search_result['status'] == 200 && count($search_result['response']['results']) < 1){
            return [
                'status' => 'warning',
                'message' => 'No se encontró el pago en Mercado Pago'
            ];
        }
        
        if ($search_result['status'] != 200 || !isset($search_result['response']['results'][0]['collection'])) {
            return [
                'status' => 'error',
                'message' => 'No se recibió respuesta del pago de Mercado Pago'
            ];
        }

        return $search_result['response']['results'];
    }

    public static function cancelPayments($mp, $payments) {
        $result = [];
     
        //si hay mas de un pago asociado al reference
        if (count($payments) > 1) {
            foreach ($payments as $key => $payment) {
                //si aun no se ha aprobado cancelo el pago,
                $result[] = $mp->cancel_payment($payment['collection']['id']);
            }
        } else {
            //si aun no se ha aprobado cancelo el pago,
            $result[] = $mp->cancel_payment($payments[0]['collection']['id']);
        }
        //devuelvo error si alguno de los pagos no se pudo cancelar, pero me aseguro que intente cancelar todos los pagos
        foreach ($result as $r) {
            if ($r['status'] != 200) {
                return [
                    'status' => 'error',
                    'message' => 'No se pudo cancelar el Pago en Mercado Pago',
                ];
            }
        }
        return [
            'status' => 'success',
            'message' => '',
        ];
    }

}

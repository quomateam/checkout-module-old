<?php

namespace quoma\checkout\platforms\MercadoPago\controllers;

use Yii;
use yii\web\Controller;
use quoma\checkout\platforms\MercadoPago\MercadoPago;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\Platform;
use quoma\checkout\models\Serialize;
use quoma\checkout\models\SiteData;
use quoma\checkout\models\Site;
use quoma\checkout\models\WebPaymentData;

class DefaultController extends Controller {

    public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
    }

    public function actionCreatePreference($payment) {

        $this->layout = '//main_no_menu';

        $result = [];
        $items = [];

        $payment = WebPayment::find()->where(['uuid' => $payment])->one();

        if (!$payment) {
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Your request is invalid: payment not found.'));
        }

        //chequeo si no ha superado el tiempo disponible para la operación
        if ($payment->isOnTimeout()) {
            return $this->render('_error', [
                        'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                        'site' => $payment->site->name,
                        'operation' => $payment->uuid
            ]);
        }

        $mercadoPago = new MercadoPago();

        $site = \quoma\checkout\models\Site::findOne($payment->site_id);


        //TODO: recibir un listado de items para armar el detalle de un pago
        $items[] = [
            "title" => $payment->concept,
            "quantity" => 1,
            "currency_id" => "ARS",
            "unit_price" => $payment->pendingAmount,
            'picture_url' => \yii\helpers\Url::to('@web/images/site/' . $payment->site->image, true),
        ];

        $excluded_payment_methods = $site->getExcludedPaymentMethods($payment->platform_id);
        $payment_methods = [
            "excluded_payment_methods" => $excluded_payment_methods,
            "excluded_payment_types" => [],
        ];

        if ($payment->installments) {
            $payment_methods ['default_installments'] = (int) $payment->installments;
            $payment_methods ['installments'] = (int) $payment->installments;
        } else {
            $payment_methods ['installments'] = (int) $site->findInstallment();
        }

        $preference_data = [
            'id' => $payment->uuid,
            'external_reference' => $payment->uuid,
            "items" => $items,
            'additional_info' => $payment->concept,
            'payer' => [
                'name' => '',
                'surname' => '',
                'email' => '',
                'phone' => [
                    'number' => ''
                ],
                'address' => [
                    'street_name' => '',
                    'zip_code' => ''
                ]
            ],
            'auto_return' => 'approved',
            'payment_methods' => $payment_methods,
            "back_urls" => [
                "success" => $mercadoPago->getCallbackUrl(),
                "failure" => $mercadoPago->getCallbackUrl(),
                "pending" => $mercadoPago->getCallbackUrl()
            ],
        ];
        
        if($payment->expires){
            $expiration_date = $payment->getExpirationDateTo();
            if($expiration_date != NULL){
                $preference_data['expires'] = ($payment->expires == 1) ? true : false;
                $preference_data['expiration_date_to'] = $payment->getExpirationDateTo();
            }
        }
        
        try {
            $preference = $mercadoPago->getMercadoPago($site->site_id)->create_preference($preference_data);
            $url = $preference['response'][(\Yii::$app->params['mp_sandbox'] ? "sandbox_" : "") . 'init_point'];
        } catch (\Exception $ex) {
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Payment broker error.'));
        }


        return $this->render('init', [
                    'preference' => $preference_data,
                    'payment' => $payment,
                    'url' => $url,
        ]);
    }

    public function actionCreatePreapprovalPayment($payment) {

        $this->layout = '//main_no_menu';

        $result = [];
        $items = [];

        $payment = WebPayment::find()->where(['uuid' => $payment])->one();

        if (!$payment) {
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Your request is invalid: payment not found.'));
        }

        //chequeo si no ha superado el tiempo disponible para la operación
        if ($payment->isOnTimeout()) {
            return $this->render('_error', [
                        'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                        'site' => $payment->site->name,
                        'operation' => $payment->uuid
            ]);
        }

        $mercadoPago = new MercadoPago();

        $site = \quoma\checkout\models\Site::findOne($payment->site_id);

        $url = $mercadoPago->getCallbackUrl() . "&site_id=$site->site_id";
        
        if(Yii::$app->params['testing']){
            $url = \Yii::$app->params['mp_back_url'] . "index.php?r=checkout_online/MercadoPago/default/back&site_id=$site->site_id";
        }
        $preapproval_data = [
            "payer_email" => $payment->payer_email,
            //debe ser una url publica
            "back_url" => $url,
            "reason" => $payment->reason,
            "external_reference" => uniqid('', true) . '-' . $payment->uuid,
            "auto_recurring" => array(
                "frequency" => $payment->frequency,
                "frequency_type" => $payment->frequency_type,
                "transaction_amount" => $payment->amount,
                "currency_id" => $payment->currency_id,
            )
        ];
        
        if ($payment->payment_start_date) {
            $preapproval_data['auto_recurring']['start_date'] = $payment->payment_start_date;
        }

        if ($payment->payment_end_date) {

            $preapproval_data['auto_recurring']['end_date'] = $payment->payment_end_date;
        }
       
        try {
            //creo la subscripción en Cobros
            $subscription = WebPayment::getSubscription($preapproval_data);
            $payment->subscription_id= $subscription->subscription_id;
            $payment->save(false,['subscription_id']);
           
            $preapproval = $mercadoPago->getMercadoPago($site->site_id)->create_preapproval_payment($preapproval_data);
            $url = $preapproval['response'][(\Yii::$app->params['mp_sandbox'] ? "sandbox_" : "") . 'init_point'];
        } catch (\Exception $ex) {
            $message = 'Payment broker error.';
            if (!YII_ENV_PROD) {
                $message .= ' ' . $ex->getMessage();
            }
            throw new \yii\web\HttpException(400, Yii::t('yii', $message));
        }

        return $this->render('init', [
                    'payment' => $payment,
                    'url' => $url,
        ]);
    }

    /**
     * Chequea el tiempo que el cliente se demoró en la vista de cobros para no enviar pagos que puedn superar el tiempo máximo de operación
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCheckTime() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];

        if ($post = \Yii::$app->request->post()) {

            $web_payment = WebPayment::findOne($post['payment_id']);

            if (!$web_payment) {
                $json['status'] = 'error';
                $json['message'] = 'El pago no existe!';
                return $json;
            }

            //chequeo si no ha superado el tiempo disponible para la operación
            if ($web_payment->isOnTimeout()) {
                $web_payment->changeStatus('timeout');
                $this->notifyPaymentTimeOut($web_payment);
                $json['status'] = 'error';
                $json['html'] = $this->renderPartial('_error', [
                    'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                    'site' => $web_payment->site->name,
                    'operation' => $web_payment->uuid
                ]);
                return $json;
            }

            $json['status'] = 'success';
            $json['message'] = 'OK';

            return $json;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function NotifyPaymentTimeOut($payment) {

        if (isset($payment->request_uuid))
            $nro_operation = $payment->request_uuid;
        else
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $nro_operation, 'status' => $payment->status]);

        if (\Yii::$app->params['testing']) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        //execute post
        $result = curl_exec($ch);
        
        if ($result === false) {
            echo "Error: " . curl_error($ch);
            echo "\n";
        }
        //close connection
        curl_close($ch);
        
//        \Yii::$app->response->format = 'json';
        return [
            'status' => 'ok',
            'message' => '',
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionResult() {
       
        Serialize::saveData($_GET, $_GET["site_id"]);
        $site_id = $_GET["site_id"];
        $topic = $_GET["topic"];
        $payment_id = $_GET["id"];
        $merchant_order_info = null;
        
        $platform = MercadoPago::getPlatform();
        $site = Site::findOne($site_id);

        if (!isset($_GET["id"]) || !isset($platform) || !isset($site)) {
            http_response_code(400);
            return;
        }

        $secret = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'secret');
        $client_id = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'client_id');

        $mp = new \MP($client_id, $secret);

        if ($topic == 'preapproval') {
           
            //obtengo la información del pago
            $payment_info = $mp->get_preapproval_payment($payment_id);
            //busco la subscripción sino la creo
            $subscription = WebPayment::getSubscription($payment_info['response']);
            $payment = WebPayment::find()->where(['uuid' => WebPayment::getPaymentUuid($payment_info['response']['external_reference'])])->one();
            
            if (isset($payment->request_uuid)) {
                $nro_operation = $payment->request_uuid;
            } else {
                throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));
            }
           
            $result = WebPayment::NotifyToSite($payment, ['data' => $nro_operation, 'status' => $subscription->status, 'subscription_uuid' => $subscription->uuid, 'is_subscription' => true, 'type' => $topic]);
        } elseif ($topic == 'payment') {
            //obtengo la información del pago
            $payment_info = $mp->get("/collections/notifications/$payment_id");
            
            if ($payment_info["response"]["collection"]["merchant_order_id"] != '') {
                $merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);

                if ($merchant_order_info == null) {
                    echo "Error obtaining the merchant_order";
                    die();
                }

                if ($merchant_order_info["status"] != 200) {

                    Yii::$app->response->statusCode = 500;
                    return;
                }
            }
            

            $model = new \quoma\checkout\platforms\MercadoPago\ResultModel();
            $model->setAttributes($payment_info["response"]["collection"]);
            
            $customer = new \quoma\checkout\platforms\MercadoPago\ResultModel();
            $customer->setAttributes($payment_info["response"]["collection"]['payer']);


            $response = [
                'status' => $model->status,
                'amount' => (float) (str_replace(',', '.', $model->transaction_amount)),
                'currency' => $model->currency_id,
                'autorization_code' => '',
                'installments' => $model->installments,
                'owner' => $customer->first_name . ' ' . $customer->lastname,
                'card' => '',
                'email' => $customer->email,
                'datetime' => date('Y-m-d H:i:s', strtotime($model->date_approved)),
                'payment' => $model->external_reference,
                'messages' => ''
            ];
            
            if($payment_info["response"]["collection"]["operation_type"] == 'recurring_payment'){
                $is_subscription = true;
              
                //TODO: el negocio no debe conocer al controlador
                $payment = $this->loadPaymentPreaproval(WebPayment::getPaymentUuid($model->external_reference),$payment_info['response']['collection']['id'], $site);
            }else{
                $is_subscription = false;
                $payment = $this->loadPayment($model->external_reference);
            }
           
            $payment->receipt($response);
            WebPaymentData::savePaymentData($topic, $payment_id, $payment->web_payment_id);
           
            if (isset($payment->request_uuid)) {
                $nro_operation = $payment->request_uuid;
            } else {
                throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));
            }
           
            $result = WebPayment::NotifyToSite($payment, ['data' => $nro_operation, 'remote_uuid' => $payment->uuid,'status' => $payment->status, 'response' => serialize($response), 'is_subscription' => $is_subscription , 'type' => $topic, 'operation_type' => $payment_info['response']['collection']['operation_type']]);
        }

        Yii::$app->response->statusCode = 200;
        return 'ok';
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPayment($uuid) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        return $model;
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPaymentPreaproval($uuid, $external_id,$site) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();
       
        if($model){
            //busco  el id del pago en mp
            $web_payment_data = WebPaymentData::find()->where(['external_payment_id' => $external_id])->one();
            
            $group_payments  = WebPayment::find()->where(['request_uuid' => $model->request_uuid])->one();
            $count  = WebPayment::find()->where(['request_uuid' => $model->request_uuid])->count();
            
            if($web_payment_data 
                    || ($count == 1 && !WebPaymentData::find()->where(['web_payment_id' => $group_payments->web_payment_id])->one())){
                $model = WebPayment::findOne($model->web_payment_id);
                return $model;
            }
        }
       
        if(!isset($web_payment_data) || $model === null){
            $new_model = new \quoma\checkout\models\WebPayment();
            $new_model->load($model);
            $new_model->link('site', $site);
            $new_model->platform_id = $model->platform_id;
            $new_model->return_url = $model->return_url;
            $new_model->result_url = $model->result_url;
            $new_model->request_uuid = $model->request_uuid;
            $new_model->installments = $model->installments;
            $new_model->payer_email = $model->payer_email;
            $new_model->frequency = $model->frequency;
            $new_model->frequency_type = $model->frequency_type;
            $new_model->reason = $model->reason;
            $new_model->preapproval_payment = $model->preapproval_payment;
            $new_model->amount = $model->amount;
            $new_model->type = $model->type;

            if (!$new_model->save()) {
                throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
            }
        }
        return $new_model;
    }

    public function actionBack() {
        $is_subscription = false;
        
        if (isset($_GET['external_reference'])) {
            $operation = $_GET['external_reference'];
        } elseif (isset($_GET['preapproval_id'])) {
            $is_subscription = true;
            $payment_info = WebPayment::getPaymentInfo($_GET["site_id"], $_GET['preapproval_id']);
            $operation = WebPayment::getPaymentUuid($payment_info['response']['external_reference']);
        }
       
        //Buscamos el pago asociado
        $payment = WebPayment::find()->where(['uuid' => $operation])->one();

        if ($payment) {

            if (isset($payment->request_uuid))
                $operation = $payment->request_uuid;
            else
                throw new \yii\web\HttpException(500, 'The operation does not exist.');

            //redirecciono 
            $return_url = $payment->return_url;
            $status = ($is_subscription)?$payment_info['response']['status']:$payment->status;
            if (strpos($return_url, '?') === false)
                $url = "$return_url?data=$operation&status=$status";
            else
                $url = "$return_url&data=$operation&status=$status";

            return Yii::$app->response->redirect($url);
        }else {
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        }
    }

}

<h3 class="text-center text-danger"><?= $message ?></h3>

<div class="row">
    <div class="col-lg-12 text-center">
        <a id="back_button" class="button btn btn-primary send-form" href="<?= Yii::$app->urlManager->createUrl(['checkout_online/MercadoPago/default/back', 'external_reference' => $operation]) ?>">Volver a <?= $site ?></a>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Payment */

$this->title = quoma\checkout\CheckoutModule::t('Pago');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['#']];
//$this->params['breadcrumbs'][] = $this->title;
?>


<div class="checkout-default-index">

    <div class="panel-body">

        <div id="column-content" class="col-lg-3 text-center">
            <img style="height: 120px" src="<?= yii\helpers\Url::to('@web/images/site/' . $payment->site->image, true) ?>" class="img-responsive inline-block">
        </div>
        <div class="col-lg-9" style="border-left: 2px solid #ccc;">
            <?php if ($payment->preapproval_payment): ?>

                <h3>
                    <?php
                    //TODO: implementar i18n a nivel plataforma
                    echo Yii::t('app', 'Monto Mensual');
                    ?>: $ <?php echo Yii::$app->formatter->asCurrency($payment->pendingAmount, 'ARG'); ?> 
                </h3>
                <span>Forma de pago:</span><h4>Débito automático</h4>
                <br>
                <a id="MP-Checkout" class="btn btn-success">Suscribirse y Pagar</a>
                <h6> Al presionar "Suscribirse y Pagar" lo redirigiremos a Mercado Pago.</h6>
                
                <br>
                
                <!--<a href="<?php // echo $url;  ?>" name="MP-Checkout" class="red-ar-l-rn-ArOn" mp-mode="redirect">Pagar</a>-->
                <h5>Dispone de <strong><span id="count-down" class="text-warning"></span></strong>  minutos para continuar. </h5>
            <?php else: ?>
                <h3>
                    <?php
                    //TODO: implementar i18n a nivel plataforma
                    echo Yii::t('app', 'Monto Total');
                    ?>: $ <?php echo Yii::$app->formatter->asCurrency($payment->pendingAmount, 'ARG'); ?> 
                </h3>
                <br>
                <a id="MP-Checkout" class="btn btn-success">Pagar</a>
                <h6> Al presionar "Pagar" lo redirigiremos a Mercado Pago.</h6>
                
                <br>
                
                <h5>Dispone de <strong><span id="count-down" class="text-warning"></span></strong>   minutos para continuar. </h5>

                <a href="https://www.mercadopago.com.ar/promociones" target="_blank">
                    Ver Promociones de Pago
                </a>
            <?php endif; ?>

        </div>

    </div>
    <?php if (!YII_ENV_PROD) { ?>
        <div id="request_uuid"><?php echo $payment->request_uuid; ?></div>
        <div id="cobros_url"><?php echo yii\helpers\Url::to(['/'], true); ?></div>
    <?php } ?>
</div>

<style>

    #column-content img {
        display: inline-block;
    }

</style>

<script>
    var Form = new function (event) {

        //cheque si no superó el tiempo establecido antes de enviar el pago a Decidir
        this.send = function (event) {

            $('#MP-Checkout').on('click', function () {

                checkTime();

            });
        };

        function checkTime() {

            var data = new Object;
            data.payment_id = <?= $payment->web_payment_id ?>;

            console.log(data.payment_id);
            $.ajax({
                url: '<?= yii\helpers\Url::toRoute(['default/check-time']); ?>',
                data: data,
                dataType: 'json',
                type: 'post'
            }).done(function (json) {
                if (json.status === 'success') {
                    $MPC.openCheckout({
                        url: "<?php echo $url; ?>",
                        mode: "redirect",
                    });
                } else {
                    $('.checkout-default-index').html(json.html);
                }
            });
        }
        ;

        var c = <?= Yii::$app->params['time_cobros'] - 1 ?>;
        var t;
        var timer_is_on = 0;

        this.startTime = function () {
            startCount();
        };

        function timedCount() {
            document.getElementById("count-down").innerHTML = c;

            if (c === 0) {
                console.log('c=0');

                stopCount();
                checkTime();
            }
            c = c - 1;
            t = setTimeout(function () {
                timedCount()
            }, 60000);
        }

        function startCount() {
            if (!timer_is_on) {
                timer_is_on = 1;
                timedCount();
            }
        }

        function stopCount() {
            clearTimeout(t);
            timer_is_on = 0;
        }

    };


</script>

<?php
$this->registerJs('Form.send();');
$this->registerJs("Form.startTime();");
$this->registerJsFile("https://mp-tools.mlstatic.com/buttons/render.js");


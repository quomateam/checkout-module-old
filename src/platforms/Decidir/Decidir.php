<?php

namespace quoma\checkout\platforms\Decidir;

use Yii;
/**
 * Punto de acceso a la plataforma Decidir
 * 
 * @author gabriela
 */
class Decidir extends \quoma\checkout\components\Platform implements \quoma\checkout\components\CheckoutInterface{

    /**
     * Relativo a platforms.Decidir
     * @var type 
     */
    protected $assetsPathAlias = null;
    private $_baseUrl;
    public $config;
  

    public function __construct($owner = null) {

        $this->config = include 'config.php';
                        
        parent::__construct($owner);
    }

    public function pay($payment) {
        
        return Yii::$app->response->redirect(
                Yii::$app->urlManager->createUrl(['checkout_online/Decidir/default/init', 'payment' => $payment->uuid])
        );
    }

    public function renderForm($payment) {

        $this->render('init', array('payment' => $payment));
    }

    public function getUrl($payment) {

        return $this->_baseUrl;
    }

    public function getCallbackUrl() {
        if(\Yii::$app->params['only_secure_connection']){
            return \Yii::$app->urlManager->createAbsoluteUrl('','https') . 'checkout_online/Decidir/default/result';
        }else{
            return \Yii::$app->urlManager->createAbsoluteUrl('','http') . 'checkout_online/Decidir/default/result';
        }
    }

//    public static function actions() {
//
//        $decidir = new Decidir();
//       
//        return array(
//            'Init' => $decidir->getPathAlias() . '.Init',
//            'Result' => $decidir->getPathAlias() . '.Result',
//            'Back' => $decidir->getPathAlias() . '.Back',
//        );
//    }

}

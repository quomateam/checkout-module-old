<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Site */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'admin_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'server_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'enabled' => quoma\checkout\CheckoutModule::t('Enabled'), 'disabled' => quoma\checkout\CheckoutModule::t('Disabled'),]) ?>

    <?= $form->field($model, 'company_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\sale\models\Company::find()->where('status="enabled"')->all(), 'company_id', 'name'), ['prompt' => 'Seleccione en caso de que desee facturar desde este sitio']) ?>


    <?=
    $form->field($model, 'platforms')->checkboxList(
            \yii\helpers\ArrayHelper::map(
                    \quoma\checkout\models\Platform::find()->where('status="enabled"')->all(), 'platform_id', 'name'
            ), [
        'item' => function ($index, $label, $name, $checked, $value) use ($model) {
            return Html::checkbox($name, (in_array($value, $model->fillPlataforms()) ? true : false), [
                        'value' => $value,
                        'label' => $label,
                        'data-target' => 'view-' . $value,
                        'class' => 'check_platform',
                    ]) . '<br/>'
                    . '<div id="view_' . $value . '"></div>';
        }
                    ], [
                'encode' => false,
                    ]
            );
            ?>


            <div class="form-group<?php if ($model->hasErrors('paymentMethods')) echo ' has-error' ?>">



            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Imagen</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <?php if (!$model->isNewRecord && !empty($model->image)): ?>
                                <div class="col-lg-12">
                                    <table class="table table-responsive detail-view">
                                        <thead>
                                            <tr>
                                                <td class="col-lg-6"><img src="<?= yii\helpers\Url::to('@web/images/site/' . $model->image, true) ?>" alt="" style="width: 80%" class="img-rounded"></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            <?php endif; ?>

                            <?php
                            echo \kartik\file\FileInput::widget([
                                'name' => 'Site[image]',
                                'options' => [
                                    'multiple' => false
                                ],
                                'pluginOptions' => [
//                        'maxFileCount' => 10,
                                    'showUpload' => false
                                ]
                            ]);
                            ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

        <script>



            var Platforms = new function () {

                this.init = function () {

                    $(document).ready(function () {

                        $("input:checkbox[class=check_platform]:checked").each(function () {

                            var data = new Object;
                            data.select = $(this).val();
                            data.site_id = <?= ($model->site_id) ? $model->site_id : NULL ?>

                            $.ajax({
                                url: '<?= yii\helpers\Url::toRoute(['site/get-config']); ?>',
                                data: data,
                                dataType: 'json',
                                type: 'post'
                            }).done(function (json) {
                                if (json.status === 'success') {
                                    var name = '#view_' + json.platform_id;
                                    $(name).html(json.html);
                                }
                            });
                        });
                    });
                };

                this.change = function (event) {
                    $('.check_platform').on('change', function () {

                        var data = new Object;
                        data.select = $(this).val();

                        if ($(this).is(':checked')) {

                            $.ajax({
                                url: '<?= yii\helpers\Url::toRoute(['site/get-config']); ?>',
                                data: data,
                                dataType: 'json',
                                type: 'post'
                            }).done(function (json) {

                                if (json.status === 'success') {
                                    var name = '#view_' + json.platform_id;
                                    $(name).html(json.html);
                                }
                            });
                        } else {
                            var name = '#view_' + data.select;
                            $(name).html('');
                        }

                    });
                };

                this.methods_type = function (platform) {
                
                    $('.detail_methods').on('change', function () {
                        
                        var id = $(this).attr('data-id');
                       
                        if ($(this).is(':checked')) {

                            $('#methods_types_'+id).show();

                        } else {
                            $('#methods_types_'+id).hide();
                        }

                    });
                };
            };


        </script>

        <?php
        $this->registerJs('Platforms.init();');
        $this->registerJs('Platforms.change();');
        $this->registerJs('Platforms.methods_type();');

        
<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$model = NULL;
if ($site_id) {
    $model = quoma\checkout\models\Site::findOne($site_id);
}
?>

<div class="panel panel-primary">

    <div class="panel-heading">
        <h3 class="panel-title">Configuración</h3>
    </div>
    <div class="panel-body">

        <div class="col-sm-6">

            <h4>Métodos de pago permitidos</h4>
            <hr>
            <?=
            \yii\helpers\Html::checkboxList(
                    'Site[platforms_config][' . $platform_id . '][payment_methods]', ($model) ? $model->fillPaymentMethods($platform_id) : '', \yii\helpers\ArrayHelper::map($payment_methods, 'payment_method_id', 'name'), [
                'name' => 'payment_' . $platform_id,
                'separator' => '<br/>'
            ])
            ?>
        </div>

        <div class="col-sm-6">
            <div class="row">
                <h4>Cuotas permitidas</h4>
                <hr>
                <div class="col-sm-10">
                    <?=
                    \yii\helpers\Html::checkboxList('Site[platforms_config][' . $platform_id . '][installments]', ($model) ? $model->fillInstallments($platform_id) : 'installments', \yii\helpers\ArrayHelper::map($installments, 'installment_id', 'qty'), ['name' => 'installment' . $platform_id, 'separator' => '<br/>'])
                    ?>
                </div>
            </div>


            <h4>Tiempo de vida de una operación</h4>
            <hr>
            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Tiempo de vida de una operacion</label>
                        <div class="input-group">
                            <?= \yii\helpers\Html::input('text', 'Site[platforms_config][' . $platform_id . '][time_out]', (!empty($site_has_platform) ? $site_has_platform->time_out : ''), ['class' => 'form-control']) ?>
                            <div class="input-group-addon">minutos</div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="checkbox" class="detail_methods" id="detail_methods_<?=$platform_id?>" onclick="Platforms.methods_type()" data-id="<?=$platform_id?>" <?= (!empty($model->paymentMethodTypeHasSite)) ? 'checked' :  '';?>><label for="detail_methods_<?=$platform_id?>">Detallar por medio de pago</label>

            <div id="methods_types_<?= $platform_id?>" style="<?= (!empty($model->paymentMethodTypeHasSite)) ? '' :  'display: none';?>">
                <hr>

                <?php 
                if ($payment_method_types): ?>
                    <?php foreach ($payment_method_types as $method_type): ?>
                        <div class="col-sm-8" style="padding-top: 10px"><label><?= $method_type->name ?></label></div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputAmount">Tiempo de vida de una operacion</label>
                                <div class="input-group">
                                    <?= \yii\helpers\Html::input('text', 'Site[platforms_config][' . $platform_id . '][time_outs_methods]['.$method_type->payment_method_type_id.']', 
                                            ($model)?$model->findTimeOutPaymentMethodTypes($platform_id, $method_type->payment_method_type_id):'', ['class' => 'form-control']) ?>
                                    <div class="input-group-addon">minutos</div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>


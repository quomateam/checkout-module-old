<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\checkout\CheckoutModule;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\checkout\models\search\PaymentMethodTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = quoma\checkout\CheckoutModule::t('Payment Method Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-type-index">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create').' ' . 
                    CheckoutModule::t('Payment Method Type'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'key',

            ['class' => 'quoma\core\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\PaymentMethodType */

$this->title = \Yii::t('app', 'Create').' '. quoma\checkout\CheckoutModule::t('Payment Method Type');
$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Payment Method Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

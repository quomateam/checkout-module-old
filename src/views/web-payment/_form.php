<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList([ 'init' => 'Init', 'error' => 'Error', 'paid' => 'Paid', 'pending' => 'Pending', 'partial' => 'Partial', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'concept')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'platform_id')->textInput() ?>

    <?= $form->field($model, 'return_url')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'invoice')->checkbox() ?>

    <?= $form->field($model, 'site_id')->textInput() ?>

    <?= $form->field($model, 'bill_id')->textInput() ?>

    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

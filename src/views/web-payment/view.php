<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */

$this->title = $model->web_payment_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Web Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'web_payment_id' => $model->web_payment_id, 'site_id' => $model->site_id], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($model->deletable)
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'web_payment_id' => $model->web_payment_id, 'site_id' => $model->site_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'web_payment_id',
            'status',
            'message',
            'amount',
            'uuid',
            'concept',
            'platform_id',
            'return_url:url',
            'request_uuid',
            'invoice',
            'site_id',
            'bill_id',
        ],
    ])
    ?>

    <h3>Receipt</h3>
    <?php foreach ($model->receipts as $receipt) : ?>
    <?=
        DetailView::widget([
        'model' => $receipt,
        'attributes' => [
            'web_receipt_id', // title attribute (in plain text)
            'currency', // description attribute in HTML
            'datetime',
            'autorization_code',
            'installments',
            'owner',
            'amount',
            'card',
            'email',
            'operation_number',
            'status',
            'visa_address_validation',
            'visa_vbv_auth',
            'web_payment_id'
        ],
    ]);
    
    ?>
<?php endforeach; ?>

</div>

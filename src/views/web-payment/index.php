<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel quoma\checkout\models\search\WebPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Web Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'web_payment_id',
            'status',
//            'message',
            'amount',
            'uuid',
//             'concept',
            [
                'attribute' => 'platform_id',
                'value' => function($data) {
            return (!empty($data->platform)) ? $data->platform->name : '';
        }
            ],
            [
                'attribute' => 'site_id',
                'value' => function($data) {
            return (!empty($data->site)) ? $data->site->name : '';
        }
            ],
            'bill_id',
            [
                'class' => 'quoma\core\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>

</div>

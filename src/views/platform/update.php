<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Platform */

$this->title = Yii::t('app', 'Update').' '.quoma\checkout\CheckoutModule::t('Platform'). ' ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->platform_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="platform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

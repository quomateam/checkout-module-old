<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\PaymentMethod */

$this->title = Yii::t('app', 'Update'). ' ' .\quoma\checkout\CheckoutModule::t('Payment Method') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' =>\quoma\checkout\CheckoutModule::t('Payment Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->payment_method_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-method-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

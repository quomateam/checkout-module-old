<?php

namespace quoma\checkout;

use yii\base\BootstrapInterface;
use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;

class CheckoutModule extends QuomaModule implements BootstrapInterface {

    public $controllerNamespace = 'quoma\checkout\controllers';

    public function init() {
        
        parent::init();

        $this->registerTranslations();


        $this->modules = [
            'Decidir' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\platforms\Decidir\DecidirModule',
            ],
            'MercadoPago' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\platforms\MercadoPago\MercadoPagoModule',
            ],
        ];
        \Yii::$app->setAliases(['@quoma'=> $this->basePath]);
    }

    public function getMenu(Menu $menu)
    {
        $_menu = (new Menu(Menu::MENU_TYPE_ROOT))
            ->setName('checkout_online')
            ->setLabel(self::t('Checkout Online'))
            ->setSubItems([
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Sites'))->setUrl(['/checkout_online/site/index']),
                (new Menu(Menu::MENU_TYPE_DIVIDER)),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Platforms'))->setUrl(['/checkout_online/platform/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Payment Methods'))->setUrl(['/checkout_online/payment-method/index']),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Installments'))->setUrl(['/checkout_online/installment/index']),
            ])
        ;
        $menu->addItem($_menu, Menu::MENU_POSITION_LAST);
        return $_menu;
    }

    public function getDependencies()
    {
        return [
        ];
    }

}

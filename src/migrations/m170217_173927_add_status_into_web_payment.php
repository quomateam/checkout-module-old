<?php

use yii\db\Migration;

class m170217_173927_add_status_into_web_payment extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
        CHANGE COLUMN `status` `status` ENUM('init', 'error', 'paid', 'pending', 'partial', 'timeout', 'canceled', 'rejected', 'refunded','in_mediation','charged_back') NULL DEFAULT NULL");

    }

    public function down()
    {
        echo "m170217_173927_add_status_into_web_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

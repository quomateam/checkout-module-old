<?php

use yii\db\Migration;

class m170103_210537_add_status_web_payment extends Migration {

    public function up() {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("ALTER TABLE `$db`.`web_payment` 
        CHANGE COLUMN `status` `status` ENUM('init', 'error', 'paid', 'pending', 'partial', 'timeout', 'canceled') NULL DEFAULT NULL ;");
    }

    public function down() {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        $this->execute("ALTER TABLE `$db`.web_payment CHANGE `status` `status` ENUM('init','error','paid','pending','partial') NULL DEFAULT NULL;");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

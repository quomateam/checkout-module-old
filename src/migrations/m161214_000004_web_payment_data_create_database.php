<?php

use yii\db\Migration;

class m161214_000004_web_payment_data_create_database extends Migration
{

    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout_payment');

        $this->execute("CREATE SCHEMA IF NOT EXISTS `$db` DEFAULT CHARACTER SET utf8;");

        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.web_payment_data (
            web_payment_data_id INT NOT NULL AUTO_INCREMENT,
            external_payment_id VARCHAR(100) NOT NULL,
            web_payment_id INT NOT NULL,
            attribute VARCHAR(255) NULL,
            PRIMARY KEY (web_payment_data_id))
          ENGINE = InnoDB;");
    }

    public function down()
    {
        if (!YII_ENV_PROD) {
            $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout_payment');
            $this->execute("DROP DATABASE $db");
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

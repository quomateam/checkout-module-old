<?php

use yii\db\Migration;

class m161214_000001_checkout_create_date_base extends Migration {

    public function up() {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("CREATE DATABASE `$db`");

        $this->execute("CREATE TABLE `$db`.`installment` (
            `installment_id` int(11) NOT NULL AUTO_INCREMENT,
            `key` int(11) DEFAULT NULL,
            `qty` int(11) DEFAULT NULL,
            PRIMARY KEY (`installment_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("INSERT INTO `$db`.`installment` (`installment_id`, `key`, `qty`) VALUES
            (1, 1, 1),
            (2, 3, 3),
            (3, 12, 12);");


        $this->execute("CREATE TABLE `$db`.`payment_method` (
            `payment_method_id` int(11) NOT NULL AUTO_INCREMENT,
            `code` int(11) DEFAULT NULL,
            `name` varchar(45) DEFAULT NULL,
            PRIMARY KEY (`payment_method_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



        $this->execute("CREATE TABLE `$db`.`platform` (
            `platform_id` int(11) NOT NULL AUTO_INCREMENT,
            `main_class` varchar(100) DEFAULT NULL,
            `status` enum('enabled','disabled') DEFAULT NULL,
            `name` varchar(45) DEFAULT NULL,
            PRIMARY KEY (`platform_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->insert("$db.platform", ['main_class' => 'quoma\checkout\platforms\MercadoPago\MercadoPago', 'status' => 'enabled', 'name' => 'Mercado Pago']);
        $this->insert("$db.platform", ['main_class' => 'quoma\checkout\platforms\Decidir\Decidir', 'status' => 'enabled', 'name' => 'Decidir']);

        $this->execute("CREATE TABLE `$db`.`serialize` (
            `serialize_id` int(11) NOT NULL AUTO_INCREMENT,
            `payment_id` int(11) DEFAULT NULL,
            `data` text,
            PRIMARY KEY (`serialize_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`site` (
            `site_id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(45) DEFAULT NULL,
            `server_name` varchar(100) DEFAULT NULL,
            `status` enum('enabled','disabled') DEFAULT NULL,
            `company_id` int(11) DEFAULT NULL,
            `image` varchar(255) DEFAULT NULL,
            PRIMARY KEY (`site_id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`site_has_installment` (
            `site_has_installment_id` int(11) NOT NULL AUTO_INCREMENT,
            `site_id` int(11) NOT NULL,
            `installment_id` int(11) NOT NULL,
            `platform_id` int(11) NOT NULL,
            PRIMARY KEY (`site_has_installment_id`),
            KEY `fk_site_has_installment_installment1_idx` (`installment_id`),
            KEY `fk_site_has_installment_site1_idx` (`site_id`),
            KEY `fk_site_has_installment_platform1_idx` (`platform_id`),
            CONSTRAINT `fk_site_has_installment_installment1` FOREIGN KEY (`installment_id`) REFERENCES `installment` (`installment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_site_has_installment_site1` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `site_has_installment_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`platform_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`site_has_payment_method` (
            `site_has_payment_method_id` int(11) NOT NULL AUTO_INCREMENT,
            `site_id` int(11) NOT NULL,
            `payment_method_id` int(11) NOT NULL,
            `platform_id` int(11) NOT NULL,
            PRIMARY KEY (`site_has_payment_method_id`),
            KEY `fk_site_has_payment_method_payment_method1_idx` (`payment_method_id`),
            KEY `fk_site_has_payment_method_site1_idx` (`site_id`),
            KEY `fk_site_has_payment_method_platform1_idx` (`platform_id`),
            CONSTRAINT `fk_site_has_payment_method_payment_method1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`payment_method_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_site_has_payment_method_platform1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`platform_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_site_has_payment_method_site1` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`site_has_platform` (
            `site_id` int(11) NOT NULL,
            `platform_id` int(11) NOT NULL,
            `time_out` int(11) DEFAULT NULL,
            PRIMARY KEY (`site_id`,`platform_id`),
            KEY `fk_site_has_platform_platform1_idx` (`platform_id`),
            KEY `fk_site_has_platform_site_idx` (`site_id`),
            CONSTRAINT `fk_site_has_platform_platform1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`platform_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_site_has_platform_site` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`web_payment` (
            `web_payment_id` int(11) NOT NULL AUTO_INCREMENT,
            `status` enum('init','error','paid','pending','partial') DEFAULT NULL,
            `message` varchar(255) DEFAULT NULL,
            `amount` float DEFAULT NULL,
            `uuid` varchar(45) DEFAULT NULL,
            `concept` varchar(255) DEFAULT NULL,
            `platform_id` int(11) DEFAULT NULL,
            `return_url` varchar(100) DEFAULT NULL,
            `invoice` tinyint(1) DEFAULT '0',
            `site_id` int(11) NOT NULL,
            `bill_id` int(11) DEFAULT NULL,
            `result_url` varchar(100) DEFAULT NULL,
            `request_uuid` varchar(45) DEFAULT NULL,
            PRIMARY KEY (`web_payment_id`,`site_id`),
            KEY `fk_web_payment_platform1_idx` (`platform_id`),
            KEY `fk_web_payment_site1_idx` (`site_id`),
            CONSTRAINT `fk_web_payment_platform1` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`platform_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT `fk_web_payment_site1` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

        $this->execute("CREATE TABLE `$db`.`web_receipt` (
            `web_receipt_id` int(11) NOT NULL AUTO_INCREMENT,
            `currency` varchar(45) DEFAULT NULL,
            `datetime` datetime DEFAULT NULL,
            `autorization_code` int(11) DEFAULT NULL,
            `installments` int(11) DEFAULT NULL,
            `owner` varchar(255) DEFAULT NULL,
            `amount` float DEFAULT NULL,
            `card` varchar(45) DEFAULT NULL,
            `email` varchar(45) DEFAULT NULL,
            `operation_number` varchar(45) DEFAULT NULL,
            `status` varchar(10) DEFAULT NULL,
            `visa_address_validation` varchar(10) DEFAULT NULL,
            `visa_vbv_auth` int(11) DEFAULT NULL,
            `messages` varchar(255) DEFAULT NULL,
            `web_payment_id` int(11) NOT NULL,
            PRIMARY KEY (`web_receipt_id`),
            KEY `fk_web_receipt_web_payment1_idx` (`web_payment_id`),
            CONSTRAINT `fk_web_receipt_web_payment1` FOREIGN KEY (`web_payment_id`) REFERENCES `web_payment` (`web_payment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }

    public function down() {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        if (!YII_ENV_PROD) {
            $this->execute("DROP DATABASE $db");
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

<?php

use yii\db\Migration;

class m170216_151156_add_web_payment_data extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
        ADD COLUMN `payer_email` VARCHAR(45) NULL DEFAULT NULL AFTER `update_datetime`,
        ADD COLUMN `frequency` INT(11) NULL DEFAULT NULL AFTER `payer_email`,
        ADD COLUMN `frequency_type` VARCHAR(15) NULL DEFAULT NULL AFTER `frequency`,
        ADD COLUMN `currency_id` VARCHAR(3) NULL DEFAULT NULL AFTER `frequency_type`,
        ADD COLUMN `payment_start_date` VARCHAR(50) NULL DEFAULT NULL AFTER `currency_id`,
        ADD COLUMN `payment_end_date` VARCHAR(50) NULL DEFAULT NULL AFTER `payment_start_date`,
        ADD COLUMN `reason` VARCHAR(100) NULL DEFAULT NULL AFTER `payment_end_date`,
        ADD COLUMN `external_status` VARCHAR(45) NULL DEFAULT NULL AFTER `payment_end_date`");

    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        $this->dropColumn("$db.`web_payment`", 'payer_email');
        $this->dropColumn("$db.`web_payment`", 'frequency');
        $this->dropColumn("$db.`web_payment`", 'frequency_type');
        $this->dropColumn("$db.`web_payment`", 'currency_id');
        $this->dropColumn("$db.`web_payment`", 'payment_start_date');
        $this->dropColumn("$db.`web_payment`", 'payment_end_date');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

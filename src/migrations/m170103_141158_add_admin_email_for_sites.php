<?php

use yii\db\Migration;

class m170103_141158_add_admin_email_for_sites extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`site` 
        ADD COLUMN `admin_email` VARCHAR(45) NULL DEFAULT NULL");

    }

    public function down()
    {
         $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
         
         $this->dropColumn("$db.site", 'admin_email');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

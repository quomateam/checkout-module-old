<?php

use yii\db\Migration;

class m161220_173837_checkout_create_tables_for_payment_methods_types extends Migration {

    public function up() {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("ALTER TABLE `$db`.`payment_method` 
        ADD COLUMN `payment_method_type_id` INT(11) NOT NULL AFTER `accreditation_time`,
        ADD INDEX `fk_payment_method_payment_method_type1_idx` (`payment_method_type_id` ASC)");

        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`payment_method_type` (
            `payment_method_type_id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(45) NULL DEFAULT NULL,
            `key` VARCHAR(45) NULL DEFAULT NULL,
            PRIMARY KEY (`payment_method_type_id`))
          ENGINE = InnoDB
          DEFAULT CHARACTER SET = utf8");

        $this->insert("$db.`payment_method_type`", ['name' => 'Tarjeta de crédito','key' => 'credit_card']);
        $this->insert("$db.`payment_method_type`", ['name' => 'Ticket impreso','key' => 'ticket']);
        $this->insert("$db.`payment_method_type`", ['name' => 'Pago por cajero automático(ATM)','key' => 'atm']);
        $this->insert("$db.`payment_method_type`", ['name' => 'Pago con tarjeta de débito','key' => 'debit_card']);
        $this->insert("$db.`payment_method_type`", ['name' => 'Pago por tarjeta prepago','key' => 'prepaid_card']);

        $this->execute("UPDATE `$db`.`payment_method` SET
                `payment_method_type_id` = (
                    SELECT pmt.`payment_method_type_id`
                    FROM `$db`.`payment_method_type` pmt
                    WHERE pmt.`key` = `payment_method_type`
                )");

        $this->execute("ALTER TABLE `$db`.`payment_method`
            DROP COLUMN `payment_method_type`");

        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`payment_method_type_has_site` (
        `payment_method_type_id` INT(11) NOT NULL,
        `site_id` INT(11) NOT NULL,
        `platform_id` INT(11) NOT NULL,
        `time_out` INT(11) NULL DEFAULT NULL,
        PRIMARY KEY (`payment_method_type_id`, `site_id`, `platform_id`),
        INDEX `fk_payment_method_type_has_site_site1_idx` (`site_id` ASC),
        INDEX `fk_payment_method_type_has_site_payment_method_type1_idx` (`payment_method_type_id` ASC),
        INDEX `fk_payment_method_type_has_site_platform1_idx` (`platform_id` ASC),
        CONSTRAINT `fk_payment_method_type_has_site_payment_method_type1`
        FOREIGN KEY (`payment_method_type_id`)
        REFERENCES `$db`.`payment_method_type` (`payment_method_type_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
        CONSTRAINT `fk_payment_method_type_has_site_site1`
        FOREIGN KEY (`site_id`)
        REFERENCES `$db`.`site` (`site_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
        CONSTRAINT `fk_payment_method_type_has_site_platform1`
        FOREIGN KEY (`platform_id`)
        REFERENCES `$db`.`platform` (`platform_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
        ENGINE = InnoDB
        DEFAULT CHARACTER SET = utf8");
        
        $this->execute("ALTER TABLE `$db`.`payment_method`
        ADD CONSTRAINT `fk_payment_method_payment_method_type1`
        FOREIGN KEY (`payment_method_type_id`)
        REFERENCES `$db`.`payment_method_type` (`payment_method_type_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION");
    }

    public function down() {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("SET FOREIGN_KEY_CHECKS=false;");
        
        $this->execute("ALTER TABLE `$db`.`payment_method` 
        DROP FOREIGN KEY `fk_payment_method_payment_method_type1`");
        
        $this->execute("ALTER TABLE `$db`.`payment_method` 
        DROP COLUMN `payment_method_type_id`,
        DROP COLUMN `payment_method_type`,
        ADD COLUMN `payment_method_type` VARCHAR(45) NULL DEFAULT NULL AFTER `name`,
        DROP INDEX `fk_payment_method_payment_method_type1_idx` ");
        
        $this->execute("DROP TABLE `$db`.payment_method_type");
        $this->execute("DROP TABLE `$db`.payment_method_type_has_site");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

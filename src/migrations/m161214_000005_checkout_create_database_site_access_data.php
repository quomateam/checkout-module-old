<?php

use yii\db\Migration;

class m161214_000005_checkout_create_database_site_access_data extends Migration
{

    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout_site_access');

        $this->execute("CREATE SCHEMA IF NOT EXISTS `$db` DEFAULT CHARACTER SET utf8;");

        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`site_data` (
            `site_data_id` INT NOT NULL AUTO_INCREMENT,
            `site_id` INT NOT NULL,
            `attribute` VARCHAR(45) NULL,
            `value` VARCHAR(45) NULL,
            `type` VARCHAR(10) NULL,
            `platform_id` INT NOT NULL,
            PRIMARY KEY (`site_data_id`))
          ENGINE = InnoDB");
    }

    public function down()
    {
        if (!YII_ENV_PROD) {
            $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout_site_access');
            $this->execute("DROP SCHEMA IF EXISTS $db");
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

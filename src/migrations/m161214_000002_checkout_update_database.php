<?php

use yii\db\Migration;

class m161214_000002_checkout_update_database extends Migration {

    public function up() {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("ALTER TABLE `$db`.`site_has_installment` 
            DROP FOREIGN KEY `site_has_installment_ibfk_1`");

        $this->execute("ALTER TABLE `$db`.`payment_method` 
        CHANGE COLUMN `code` `code` VARCHAR(45) NULL DEFAULT NULL ,
        ADD COLUMN `payment_method_type` VARCHAR(45) NULL DEFAULT NULL AFTER `name`,
        ADD COLUMN `platform_id` INT(11) NOT NULL AFTER `payment_method_type`,
        ADD COLUMN `accreditation_time` INT(11) NULL DEFAULT NULL AFTER `platform_id`,
        ADD INDEX `fk_payment_method_platform1_idx` (`platform_id` ASC);
        ");

        $this->execute("SET FOREIGN_KEY_CHECKS=false;");

        $this->execute("ALTER TABLE `$db`.`site_has_payment_method` 
        ADD COLUMN `time_out` INT(11) NULL DEFAULT NULL AFTER `platform_id`;
        ");


        $this->execute("ALTER TABLE `$db`.`payment_method` 
        ADD CONSTRAINT `fk_payment_method_platform1`
          FOREIGN KEY (`platform_id`)
          REFERENCES `$db`.`platform` (`platform_id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION");

        $this->execute("ALTER TABLE `$db`.`site_has_installment` 
        ADD CONSTRAINT `fk_site_has_installment_platform1`
          FOREIGN KEY (`platform_id`)
          REFERENCES `$db`.`platform` (`platform_id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION");

        $this->execute("ALTER TABLE `$db`.`web_payment` 
        ADD COLUMN `installments` INT(11) NULL DEFAULT NULL
        ");
    }

    public function down() {
        echo "m161214_191639_checkout_update_database cannot be reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
